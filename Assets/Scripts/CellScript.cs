﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CellScript : MonoBehaviour
{
    [Header("Ссылки на объекты")]
    public GameController gc;
    public Image FrontImage;
    public Image BackImage;
    public Sprite FlagImage;

    [Header("Значения для анимаций")]
    public int countFramesToFlip;
    public int countFramesToVibrate;
    public int countFramesToSemiVibrate;

    [Header("Информация о ячейке")]
    public int i, j;
    public int CountNearBombs;

    public bool IsBomb;
    public bool IsMarked;
    public bool IsFlippedToBack;
    public bool IsActive;

    private bool needToStartRestart;

    // Стандартные методы
    void Start()
    {
        IsActive = true;
        IsFlippedToBack = false;
        IsMarked = false;
        IsBomb = false;
        needToStartRestart = false;
        CountNearBombs = 0;

    }
        
    void Update()
    {
        
    }


    // Обработка действий мыши 
    private void OnMouseEnter()
    {
        if (IsActive == true)
            SetCellScale(1.2f);

    }

    private void OnMouseExit()
    {
        if (IsActive == true)
            SetCellScale(1f);

    }

    private void OnMouseOver()
    {
        if (IsActive)
        {
            if (Input.GetMouseButtonDown(0))
                ProcessLMB();

            if (Input.GetMouseButtonDown(1))
                ProcessRMB();
            
            if (Input.GetMouseButtonDown(2))
                ProcessMMB();
            
        }
    }


    // Обработчики нажатий мыши
    void ProcessLMB()
    {
        if (IsFlippedToBack)
            OpenNearCells();
        else
            OpenCell();

    }

    void ProcessRMB()
    {
        if (!IsFlippedToBack)
            MarkCell();

    }

    void ProcessMMB()
    {
        OpenNearCells();

    }


    // Методы работы с ячейкой    
    public void OpenCell()      // LMB
    {
        if (!gc.IsBombsPlaced)
            gc.FillInEmptyCells(i, j);

        if (IsBomb == true && IsMarked == false)
        {
            //  --- Открытие ячейки с бомбой ---

            SetActivityState(false);
            needToStartRestart = true;
            Lose_FirstStage();

        }
        else if (IsFlippedToBack == false && IsMarked == false)
        {
            //  --- Открытие ячейки без бомбы ---

            gc.AddCountMarkedCells();

            Flip(true);
            setMarkedState(false);

            if (CountNearBombs == 0)
            {
                SetCellScale(1f);
                SetActivityState(false);

                List<CellScript> nearCells = gc.GetNeighbourCells(i, j);

                for (int i = 0; i < nearCells.Count; i++)
                {
                    //OpenNearCells();
                    nearCells[i].OpenCell();
                }
            }
        }
    }   
    
    public void MarkCell()      // RMB
    {
        if (!gc.IsBombsPlaced)
        {
            OpenCell();
        }
        else if (IsMarked)
        {
            IsMarked = false;
            FrontImage.sprite = null;
        }
        else
        {
            IsMarked = true;
            FrontImage.sprite = FlagImage;
        }
    }

    void OpenNearCells()
    {
        List<CellScript> nearCells = gc.GetNeighbourCells(i, j);

        if (!gc.IsBombsPlaced)
        {
            OpenCell();

        }
        else if (!IsFlippedToBack)
        {

        }
        else if (IsEqualsBombsAndMarkersCount(nearCells))
        {
            if (IsRightMarking(nearCells))
            {
                for (int i = 0; i < nearCells.Count; i++)
                {
                    nearCells[i].OpenCell();
                    //nearCells[i].Flip(true);
                }
            }
            else
            {
                for (int i = 0; i < nearCells.Count; i++)
                {
                    if (nearCells[i].IsBomb && !nearCells[i].IsMarked)
                    {
                        // открываем первую неугаданную бомбу для запуска завершения игры
                        nearCells[i].OpenCell();
                        break;
                    }
                }
                //SetCellScale(1f);

                /*
                needToStartRestart = true;
                Lose_FirstStage();
                */
            }

        }
        else
        {
            print("markers != bombs");
            // сделать быструю вибрацию для флажков
            for (int i = 0; i < nearCells.Count; i++)
            {
                if (nearCells[i].IsMarked)
                    nearCells[i].StartVibrate(20, 4);
            }
        }
        
    }

    bool IsRightMarking(List<CellScript> nearCells)
    {
        int rightCells = 0;
        int nearBombs = 0;
        int nearMarkers = 0;

        for (int i = 0; i < nearCells.Count; i++)
        {
            if (nearCells[i].IsBomb == nearCells[i].IsMarked)
                rightCells++;

            if (nearCells[i].IsBomb)
                nearBombs++;

            if (nearCells[i].IsMarked)
                nearMarkers++;

        }

        if (nearBombs == 0)
            return false;

        // не открывает бомбы при неправильном расположении маркеров

        if (rightCells != nearCells.Count)
            return false;

        if (nearMarkers != nearBombs)
            return false;

        return true;
        /*
        if (nearBombs != 0 && rightCells == nearCells.Count && nearMarkers == CountNearBombs)
            return true;
        else
            return false;*/
    }

    bool IsEqualsBombsAndMarkersCount(List<CellScript> nearCells)
    {
        int countBombs = 0;
        int countMarkers = 0;

        for (int i = 0; i < nearCells.Count; i++)
        {
            if (nearCells[i].IsMarked) countMarkers++;
            if (nearCells[i].IsBomb) countBombs++;

        }
        return (countBombs == countMarkers);
    }

    public void Flip(bool toBackSide = true)
    {
        if (toBackSide)
        {
            IsFlippedToBack = true;

            if (IsMarked == true)
                MarkCell();

            StartFlip(0f, 180f, countFramesToFlip);
            
        }
        else
        {
            IsFlippedToBack = false;
            StartFlip(180f, 0f, countFramesToFlip);
        }
    }

    public void SetCellScale(float scaleMultiplier)
    {
        transform.localScale = new Vector3(1f * scaleMultiplier, 1f * scaleMultiplier, 0.1f);

    }


    // Методы анимаций
    void StartFlip(float startRotation, float endRotation, int countFrames = -1)
    {
        if (countFrames == -1)
            countFrames = countFramesToFlip;

        StartCoroutine(FlipAnim(startRotation, endRotation, countFrames));
    }
    
    public void StartVibrate(int countFrames = -1, int deltaFrames = -1)
    {
        if (countFrames == -1)
            countFrames = countFramesToVibrate;

        if (deltaFrames == -1)
            deltaFrames = countFramesToSemiVibrate;
        
        StartCoroutine(VibrateAnim(countFrames, deltaFrames));

    }

    IEnumerator FlipAnim(float startRotation, float endRotation, int countFrames)
    {
        float deltaRot = (endRotation - startRotation) / countFrames;

        for (int i = 0; i < countFrames; i++)
        {
            transform.Rotate(0f, deltaRot, 0f);

            yield return new WaitForEndOfFrame();
        }

        transform.rotation = Quaternion.Euler(0f, endRotation, 0f);
    }

    IEnumerator VibrateAnim(int countFrames, int deltaFrames)
    {
        SetCellScale(1f);

        float deltaScale = (1.1f - transform.localScale.x) / deltaFrames;

        for (int i = 0; i < countFrames; i+=(deltaFrames*2))
        {
            for (int j = 0; j < deltaFrames; j++)
            {
                transform.localScale += new Vector3(deltaScale, deltaScale, 0f);
                yield return new WaitForEndOfFrame();
            }

            for (int j = 0; j < deltaFrames; j++)
            {
                transform.localScale -= new Vector3(deltaScale, deltaScale, 0f);
                yield return new WaitForEndOfFrame();
            }

        }

        SetCellScale(1f);

        if (needToStartRestart)
        {
            Invoke("Lose_SecondStage", gc.AfterVibrationsRestartTime);

        }
    }

    // Геттеры
    /*
    public bool GetActivityState()
    {
        return isActive;

    }

    public bool GetIsBombState()
    {
        return isBomb;

    }

    public bool GetFlipState()
    {
        return isFlippedToBack;

    }

    public bool GetMarkedState()
    {
        return isMarked;

    }
    */


    // Сеттеры
    public void SetActivityState(bool newState)
    {
        if (!newState)
            SetCellScale(1f);

        IsActive = newState;

    }
                
    public void SetBombState(bool newState)
    {
        IsBomb = newState;

    }
                
    public void setFlipState(bool newState)
    {
        IsFlippedToBack = newState;

    }
                
    public void setMarkedState(bool newState)
    {
        IsMarked = newState;

    }

    // Полуигровые методы
    void Lose_FirstStage()
    {
        // срабатывает до анимации и служит для показа игроку раскрытых ячеек с бомбами
        gc.SetAllCellsActivityState(false);
        gc.FlipAllCellsWithBombs();

    }

    void Lose_SecondStage()
    {
        // срабатывает после анимации и служит для начала новой игры 
        gc.Restart();

    }
}
