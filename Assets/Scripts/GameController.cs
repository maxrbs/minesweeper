﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [Header("Ссылки на объекты")]
    public Camera Camera;
    public Transform CellsContainer;
    public GameObject CellPrefab;

    [Header("Настройка игры и поля")]
    public int CountCellsInLine;
    public int CountCellsInColumn;
    public float DistanceBetweenCells;
    public int CountBombs;
    public float AfterVibrationsRestartTime;
    public float AutoRestartTime;

    [Header("Иконки для ячеек")]
    public Sprite[] tiles;

    //[Header("Название папки со спрайтами (адрес)")]    <---    использовать в дальнейшем этот вариант вместо верхнего
    //public string NameOfPack;
    //private string assetsPath;

    [HideInInspector]
    public bool IsBombsPlaced;
    public int countUnbombedCells;
    public int countMarkedCells;

    private CellScript[,] cellScripts;
    private int countCells;

    // Стандартные методы
    void Start()
    {
        StartNewGame();

    }

    void Update()
    {

    }


    // Базовая работа с ячейками
    void FillWithEmptyCells()
    {
        for (int i = 0; i < CellsContainer.childCount; i++)
        {
            Destroy(CellsContainer.GetChild(i).gameObject);

        }

        float spawnPositionX = 0;
        float spawnPositionY = 0;

        int numberOfCell = 0;
        for (int i = 0; i < CountCellsInColumn; i++)
        {
            spawnPositionX = 0;

            for (int j = 0; j < CountCellsInLine; j++)
            {
                GameObject cell = Instantiate(CellPrefab, new Vector3(spawnPositionX, spawnPositionY, 0), Quaternion.identity);
                cell.transform.parent = CellsContainer;
                cell.name = "Cell " + numberOfCell++;

                CellScript buffCellScript = cell.GetComponent<CellScript>();
                buffCellScript.gc = this;
                buffCellScript.i = i;
                buffCellScript.j = j;
                buffCellScript.FlagImage = tiles[9];

                cellScripts[i, j] = buffCellScript; // добавляем ссылку на скрипт в 2мерный массив
                spawnPositionX += DistanceBetweenCells;
            }

            spawnPositionY -= DistanceBetweenCells;
        }

        countCells = numberOfCell;

        float camPosX = (CellsContainer.GetChild(CellsContainer.childCount - 1).position.x - CellsContainer.GetChild(0).position.x) / 2f;
        float camPosY = (CellsContainer.GetChild(CellsContainer.childCount - 1).position.y - CellsContainer.GetChild(0).position.y) / 2f;

        Camera.transform.position = new Vector3(camPosX, camPosY, -10f);

        if (CountCellsInColumn > CountCellsInLine)
            Camera.orthographicSize = CountCellsInColumn / 2f + 1f;
        else
            Camera.orthographicSize = CountCellsInLine / 2f + 1f;

    }

    public void FillInEmptyCells(int col = -2, int lin = -2)
    {
        PlaceBombsAtCells(col, lin);
        CalculateNumbersOnCells();
    }
    
    void PlaceBombsAtCells(int voidCol = -2, int voidLin = -2)
    {
        int placedBombs = 0;

        if (CountBombs > countCells - 9)
        {
            Debug.LogError("Too much bombs to place...");
            placedBombs = CountBombs;

        }

        while (placedBombs < CountBombs)
        {
            int col = Random.Range(0, CountCellsInColumn);
            int lin = Random.Range(0, CountCellsInLine);

            if (cellScripts[col, lin].IsBomb == false)
            {
                if (col < voidCol - 1 || col > voidCol + 1 || lin < voidLin - 1 || lin > voidLin + 1)
                {
                    cellScripts[col, lin].SetBombState(true);
                    cellScripts[col, lin].BackImage.sprite = tiles[10];
                    placedBombs++;
                    
                }
            }

        }

        IsBombsPlaced = true;
    }
    
    void CalculateNumbersOnCells()
    {
        for (int i = 0; i < CountCellsInColumn; i++)
        {
            for (int j = 0; j < CountCellsInLine; j++)
            {
                if (cellScripts[i, j].IsBomb)
                    continue;
                
                int countNearBombs = 0;

                List<CellScript> nearCells = GetNeighbourCells(i, j);

                for (int k = 0; k < nearCells.Count; k++)
                {
                    if (nearCells[k].IsBomb)
                        countNearBombs++;
                }

                cellScripts[i, j].CountNearBombs = countNearBombs;

                //assetsPath = Path.Combine(Application.streamingAssetsPath, NameOfPack);
                cellScripts[i,j].BackImage.sprite = tiles[countNearBombs];
            }
        }

    }

    void ClearField()
    {
        for (int i = 0; i < CellsContainer.childCount; i++)
            Destroy(CellsContainer.GetChild(i).gameObject);
    }


    // Расширенная работа с ячейками
    public void FlipAllCellsWithBombs()
    {
        for (int i = 0; i < CountCellsInColumn; i++)
        {
            for (int j = 0; j < CountCellsInLine; j++)
            {
                if (cellScripts[i, j].IsBomb == true)
                {
                    cellScripts[i, j].Flip(true);
                    cellScripts[i, j].StartVibrate();

                }

            }

        }

    }

    public void SetAllCellsActivityState(bool newState)
    {
        for (int i = 0; i < CountCellsInColumn; i++)
            for (int j = 0; j < CountCellsInLine; j++)
            {
                cellScripts[i, j].SetActivityState(newState);
            }

    }

    public List<CellScript> GetNeighbourCells(int i, int j)
    {
        List<CellScript> nearCells = new List<CellScript>();
        
        for (int ii = i - 1; ii <= i + 1; ii++)
        {
            for (int jj = j - 1; jj <= j + 1; jj++)
            {
                if (ii < 0 || jj < 0 || ii > CountCellsInColumn - 1 || jj > CountCellsInLine - 1 || (ii == i & jj == j))
                    continue;

                nearCells.Add(cellScripts[ii, jj]);
            }
            
        }
        
        return nearCells;
    }

    void FlipUnflippedCells(bool isFlipToBackSide)
    {
        for (int i = 0; i < CountCellsInColumn; i++)
            for (int j = 0; j < CountCellsInLine; j++)
                if (cellScripts[i, j].IsFlippedToBack != isFlipToBackSide)
                {
                    cellScripts[i, j].Flip(isFlipToBackSide);
                    
                }

    }

    void UnmarkMarkedCells()
    {
        for (int i = 0; i < CountCellsInColumn; i++)
            for (int j = 0; j < CountCellsInLine; j++)
                if (cellScripts[i, j].IsMarked == true)
                    cellScripts[i, j].MarkCell();

    }


    // Игровые методы 
    void StartNewGame()
    {
        cellScripts = new CellScript[CountCellsInColumn, CountCellsInLine];
        ClearField();
        FillWithEmptyCells();
        IsBombsPlaced = false;
        countUnbombedCells = countCells - CountBombs;
        countMarkedCells = 0;
    }

    public void Restart()
    {
        UnmarkMarkedCells();
        FlipUnflippedCells(false);
        // сделать начало новой игры по нажатию на кнопку 
        Invoke("StartNewGame", AutoRestartTime);

    }

    public void AddCountMarkedCells(int addictiveNum = 1)
    {
        countMarkedCells += addictiveNum;

        if (countMarkedCells == countUnbombedCells)
            ProcessWinEvent();

    }

    void ProcessWinEvent()
    {
        print("WIN");

        // вибрировать неоткрытыми ячейками
        for (int i = 0; i < CountCellsInColumn; i++)
            for (int j = 0; j < CountCellsInLine; j++)
                if (cellScripts[i, j].IsBomb)
                    cellScripts[i, j].StartVibrate();

        // инвоукать рестарт (в дальнейшем через ui)
        Invoke("Restart", 3f);

    }

    // Вспомогательные методы
    void CreateTableInConsole()
    {
        string abc = "";
        for (int i = 0; i < CountCellsInColumn; i++)
        {
            for (int j = 0; j < CountCellsInLine; j++)
            {
                if (!cellScripts[i, j].IsBomb)
                    abc += cellScripts[i, j].CountNearBombs.ToString() + " ";
                else
                    abc += "$ ";
            }
            abc += "\n";
        }

        print(abc);
    }

}
