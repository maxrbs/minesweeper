﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void CellScript::Start()
extern void CellScript_Start_m2187C5623EDD85CF8C15CFE183C1F10C3D004783 (void);
// 0x00000002 System.Void CellScript::Update()
extern void CellScript_Update_m90DEEF89B07C9B39B2AA05AE208D70CB16898939 (void);
// 0x00000003 System.Void CellScript::OnMouseEnter()
extern void CellScript_OnMouseEnter_mF92805A030B64CC9F4330E458D466378DC837C00 (void);
// 0x00000004 System.Void CellScript::OnMouseExit()
extern void CellScript_OnMouseExit_m43B3758544712677B636125BCBA74610CBB71CF7 (void);
// 0x00000005 System.Void CellScript::OnMouseOver()
extern void CellScript_OnMouseOver_mBA075D3E06B5B30318192F9CB569D9E8B1AE27B5 (void);
// 0x00000006 System.Void CellScript::ProcessLMB()
extern void CellScript_ProcessLMB_m481D20CA4888088580C146C5DDFC96BEA07546E8 (void);
// 0x00000007 System.Void CellScript::ProcessRMB()
extern void CellScript_ProcessRMB_m7D698686318DDAF79BC482CBBAC1464D13235996 (void);
// 0x00000008 System.Void CellScript::ProcessMMB()
extern void CellScript_ProcessMMB_mC2AF2AA51CBD7B3E015418DA0F8223DD3318669A (void);
// 0x00000009 System.Void CellScript::OpenCell()
extern void CellScript_OpenCell_m6D08D2BA5A1BBA51FCC9CF866349AD92FE3850BC (void);
// 0x0000000A System.Void CellScript::MarkCell()
extern void CellScript_MarkCell_m1CEB7D6C1528F8812859B33F66E3AF876970B361 (void);
// 0x0000000B System.Void CellScript::OpenNearCells()
extern void CellScript_OpenNearCells_m8D67AD843269C6D9711A83FE6B2900FFF42A8617 (void);
// 0x0000000C System.Boolean CellScript::IsRightMarking(System.Collections.Generic.List`1<CellScript>)
extern void CellScript_IsRightMarking_m54D63C04E0117E412BF3509668D6BB27F66F8DDB (void);
// 0x0000000D System.Boolean CellScript::IsEqualsBombsAndMarkersCount(System.Collections.Generic.List`1<CellScript>)
extern void CellScript_IsEqualsBombsAndMarkersCount_mCE82439D14A55EA840FEF814C5DC2A4763C83DEC (void);
// 0x0000000E System.Void CellScript::Flip(System.Boolean)
extern void CellScript_Flip_m2FDF12D98C578A46DA54906E33BCE6F47AA2A702 (void);
// 0x0000000F System.Void CellScript::SetCellScale(System.Single)
extern void CellScript_SetCellScale_mE30F34C461AF6953FEAE12111E0690040C8EF5C0 (void);
// 0x00000010 System.Void CellScript::StartFlip(System.Single,System.Single,System.Int32)
extern void CellScript_StartFlip_m68EDAC191366E438E78ABFB2301A1810FAD11565 (void);
// 0x00000011 System.Void CellScript::StartVibrate(System.Int32,System.Int32)
extern void CellScript_StartVibrate_m68C587BAA15ECC2C23DF1A398EEFE58ACAC07FD5 (void);
// 0x00000012 System.Collections.IEnumerator CellScript::FlipAnim(System.Single,System.Single,System.Int32)
extern void CellScript_FlipAnim_mF440633B7B3F7B578A1F18481670F9A95E0BE2B6 (void);
// 0x00000013 System.Collections.IEnumerator CellScript::VibrateAnim(System.Int32,System.Int32)
extern void CellScript_VibrateAnim_m6A77E2934C12D43DBDDA4AF2B2AAE53FCFE7C415 (void);
// 0x00000014 System.Void CellScript::SetActivityState(System.Boolean)
extern void CellScript_SetActivityState_mB96C73DD2424B3A37A0BCDFA8E93AE3B26681C18 (void);
// 0x00000015 System.Void CellScript::SetBombState(System.Boolean)
extern void CellScript_SetBombState_m34FA5F4F7839C8343B34D2AB0A2729B3EDB95F7C (void);
// 0x00000016 System.Void CellScript::setFlipState(System.Boolean)
extern void CellScript_setFlipState_mB4694F313418E59102D46FD926BB5CC584F28ABF (void);
// 0x00000017 System.Void CellScript::setMarkedState(System.Boolean)
extern void CellScript_setMarkedState_m464FBE7E2E19E707F61160CAF5D92FCFB82001A0 (void);
// 0x00000018 System.Void CellScript::Lose_FirstStage()
extern void CellScript_Lose_FirstStage_m3556D5B3504B23B58F53C76852DAA56010F540A7 (void);
// 0x00000019 System.Void CellScript::Lose_SecondStage()
extern void CellScript_Lose_SecondStage_m456CD543979BAF829C837714990F7238A55B3126 (void);
// 0x0000001A System.Void CellScript::.ctor()
extern void CellScript__ctor_m58CAC33B9D65DE6327E6BEDF4493DCB22844860C (void);
// 0x0000001B System.Void CellScript/<FlipAnim>d__32::.ctor(System.Int32)
extern void U3CFlipAnimU3Ed__32__ctor_m3C8306BE011D741F286EEF99EF338678674A5B20 (void);
// 0x0000001C System.Void CellScript/<FlipAnim>d__32::System.IDisposable.Dispose()
extern void U3CFlipAnimU3Ed__32_System_IDisposable_Dispose_mA38B1F7485A6DD070CDC1C9C30BF5A0890582A84 (void);
// 0x0000001D System.Boolean CellScript/<FlipAnim>d__32::MoveNext()
extern void U3CFlipAnimU3Ed__32_MoveNext_mA5542445EF5C18ED485121A4D9886598BE1AA931 (void);
// 0x0000001E System.Object CellScript/<FlipAnim>d__32::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFlipAnimU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE78D8A10FFED8EA5C8F65572335539A4DD0B6364 (void);
// 0x0000001F System.Void CellScript/<FlipAnim>d__32::System.Collections.IEnumerator.Reset()
extern void U3CFlipAnimU3Ed__32_System_Collections_IEnumerator_Reset_m9346BED6567910CA6F876F1A961BB0EFB5B15431 (void);
// 0x00000020 System.Object CellScript/<FlipAnim>d__32::System.Collections.IEnumerator.get_Current()
extern void U3CFlipAnimU3Ed__32_System_Collections_IEnumerator_get_Current_m208BCEA674C57BCBD6C31EF2D9FEAAAA47D199EE (void);
// 0x00000021 System.Void CellScript/<VibrateAnim>d__33::.ctor(System.Int32)
extern void U3CVibrateAnimU3Ed__33__ctor_m1C4133168D5711366DB5305F2A4FBE525697AE9F (void);
// 0x00000022 System.Void CellScript/<VibrateAnim>d__33::System.IDisposable.Dispose()
extern void U3CVibrateAnimU3Ed__33_System_IDisposable_Dispose_mFD1311276519EC7C7EF5ED437DA236ACA83C1E0B (void);
// 0x00000023 System.Boolean CellScript/<VibrateAnim>d__33::MoveNext()
extern void U3CVibrateAnimU3Ed__33_MoveNext_m9A64EB46346DA54E238B8A45066763A79E81F4FF (void);
// 0x00000024 System.Object CellScript/<VibrateAnim>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CVibrateAnimU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE1FC24545CDBE5DC666A9A403010C610013CCDC5 (void);
// 0x00000025 System.Void CellScript/<VibrateAnim>d__33::System.Collections.IEnumerator.Reset()
extern void U3CVibrateAnimU3Ed__33_System_Collections_IEnumerator_Reset_m88E0A911C29AA03E40B41362A615AC65E8D4798A (void);
// 0x00000026 System.Object CellScript/<VibrateAnim>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CVibrateAnimU3Ed__33_System_Collections_IEnumerator_get_Current_mBD8DF7B95211DD97103BFB324079B71C24AEDE8D (void);
// 0x00000027 System.Void GameController::Start()
extern void GameController_Start_mCC3F0292799528323E2217A12DB08D98CDD492AE (void);
// 0x00000028 System.Void GameController::Update()
extern void GameController_Update_mA76A2CE1F2AC1AACCDBF913CA6E1EA73DC621CD0 (void);
// 0x00000029 System.Void GameController::FillWithEmptyCells()
extern void GameController_FillWithEmptyCells_mB48BDD3791108F6107BF9F8700EF94D034AE90EF (void);
// 0x0000002A System.Void GameController::FillInEmptyCells(System.Int32,System.Int32)
extern void GameController_FillInEmptyCells_mAAC291E6668B3D54CC37DCB1EE9B368D793096D3 (void);
// 0x0000002B System.Void GameController::PlaceBombsAtCells(System.Int32,System.Int32)
extern void GameController_PlaceBombsAtCells_m59E386DFFD30EC4E86BD51BD725452EF4DD9D407 (void);
// 0x0000002C System.Void GameController::CalculateNumbersOnCells()
extern void GameController_CalculateNumbersOnCells_mE277990E457A0D9444ADCEAB22F297D5B7FA6718 (void);
// 0x0000002D System.Void GameController::ClearField()
extern void GameController_ClearField_mE9971555CA72D52E8ACD2FDB7E60DB7263A9BDFA (void);
// 0x0000002E System.Void GameController::FlipAllCellsWithBombs()
extern void GameController_FlipAllCellsWithBombs_m4202509D2C61906811F15E82256698A07BB4CC61 (void);
// 0x0000002F System.Void GameController::SetAllCellsActivityState(System.Boolean)
extern void GameController_SetAllCellsActivityState_mC3537CC4BD1207FD2A2BDBC9B2442A0A25AD494C (void);
// 0x00000030 System.Collections.Generic.List`1<CellScript> GameController::GetNeighbourCells(System.Int32,System.Int32)
extern void GameController_GetNeighbourCells_m28277D2E82B4A26521D4FD6B03727C451D3EC1DB (void);
// 0x00000031 System.Void GameController::FlipUnflippedCells(System.Boolean)
extern void GameController_FlipUnflippedCells_mAED35070EB6A6DC4C5B049A4413FD3DFDC8B26A0 (void);
// 0x00000032 System.Void GameController::UnmarkMarkedCells()
extern void GameController_UnmarkMarkedCells_m3D355AC420234321AA8BAAEB30D34F876F48B939 (void);
// 0x00000033 System.Void GameController::StartNewGame()
extern void GameController_StartNewGame_mD49E9790ABE830D71AD16E830070AD05FFF3E6BC (void);
// 0x00000034 System.Void GameController::Restart()
extern void GameController_Restart_m4AEFD85E727195AC4517C6FBE488E42A584527B4 (void);
// 0x00000035 System.Void GameController::AddCountMarkedCells(System.Int32)
extern void GameController_AddCountMarkedCells_mECEB80820F153E578871851C3F10512E337E7CA5 (void);
// 0x00000036 System.Void GameController::ProcessWinEvent()
extern void GameController_ProcessWinEvent_mF1CCF90F21440B39896B28A1A9264617469C2CD0 (void);
// 0x00000037 System.Void GameController::CreateTableInConsole()
extern void GameController_CreateTableInConsole_mE4D6A77243C1D826DB8813E715827A14168F72FB (void);
// 0x00000038 System.Void GameController::.ctor()
extern void GameController__ctor_m9D952052C0A7234373FA5531292FCA8855BE2643 (void);
static Il2CppMethodPointer s_methodPointers[56] = 
{
	CellScript_Start_m2187C5623EDD85CF8C15CFE183C1F10C3D004783,
	CellScript_Update_m90DEEF89B07C9B39B2AA05AE208D70CB16898939,
	CellScript_OnMouseEnter_mF92805A030B64CC9F4330E458D466378DC837C00,
	CellScript_OnMouseExit_m43B3758544712677B636125BCBA74610CBB71CF7,
	CellScript_OnMouseOver_mBA075D3E06B5B30318192F9CB569D9E8B1AE27B5,
	CellScript_ProcessLMB_m481D20CA4888088580C146C5DDFC96BEA07546E8,
	CellScript_ProcessRMB_m7D698686318DDAF79BC482CBBAC1464D13235996,
	CellScript_ProcessMMB_mC2AF2AA51CBD7B3E015418DA0F8223DD3318669A,
	CellScript_OpenCell_m6D08D2BA5A1BBA51FCC9CF866349AD92FE3850BC,
	CellScript_MarkCell_m1CEB7D6C1528F8812859B33F66E3AF876970B361,
	CellScript_OpenNearCells_m8D67AD843269C6D9711A83FE6B2900FFF42A8617,
	CellScript_IsRightMarking_m54D63C04E0117E412BF3509668D6BB27F66F8DDB,
	CellScript_IsEqualsBombsAndMarkersCount_mCE82439D14A55EA840FEF814C5DC2A4763C83DEC,
	CellScript_Flip_m2FDF12D98C578A46DA54906E33BCE6F47AA2A702,
	CellScript_SetCellScale_mE30F34C461AF6953FEAE12111E0690040C8EF5C0,
	CellScript_StartFlip_m68EDAC191366E438E78ABFB2301A1810FAD11565,
	CellScript_StartVibrate_m68C587BAA15ECC2C23DF1A398EEFE58ACAC07FD5,
	CellScript_FlipAnim_mF440633B7B3F7B578A1F18481670F9A95E0BE2B6,
	CellScript_VibrateAnim_m6A77E2934C12D43DBDDA4AF2B2AAE53FCFE7C415,
	CellScript_SetActivityState_mB96C73DD2424B3A37A0BCDFA8E93AE3B26681C18,
	CellScript_SetBombState_m34FA5F4F7839C8343B34D2AB0A2729B3EDB95F7C,
	CellScript_setFlipState_mB4694F313418E59102D46FD926BB5CC584F28ABF,
	CellScript_setMarkedState_m464FBE7E2E19E707F61160CAF5D92FCFB82001A0,
	CellScript_Lose_FirstStage_m3556D5B3504B23B58F53C76852DAA56010F540A7,
	CellScript_Lose_SecondStage_m456CD543979BAF829C837714990F7238A55B3126,
	CellScript__ctor_m58CAC33B9D65DE6327E6BEDF4493DCB22844860C,
	U3CFlipAnimU3Ed__32__ctor_m3C8306BE011D741F286EEF99EF338678674A5B20,
	U3CFlipAnimU3Ed__32_System_IDisposable_Dispose_mA38B1F7485A6DD070CDC1C9C30BF5A0890582A84,
	U3CFlipAnimU3Ed__32_MoveNext_mA5542445EF5C18ED485121A4D9886598BE1AA931,
	U3CFlipAnimU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE78D8A10FFED8EA5C8F65572335539A4DD0B6364,
	U3CFlipAnimU3Ed__32_System_Collections_IEnumerator_Reset_m9346BED6567910CA6F876F1A961BB0EFB5B15431,
	U3CFlipAnimU3Ed__32_System_Collections_IEnumerator_get_Current_m208BCEA674C57BCBD6C31EF2D9FEAAAA47D199EE,
	U3CVibrateAnimU3Ed__33__ctor_m1C4133168D5711366DB5305F2A4FBE525697AE9F,
	U3CVibrateAnimU3Ed__33_System_IDisposable_Dispose_mFD1311276519EC7C7EF5ED437DA236ACA83C1E0B,
	U3CVibrateAnimU3Ed__33_MoveNext_m9A64EB46346DA54E238B8A45066763A79E81F4FF,
	U3CVibrateAnimU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE1FC24545CDBE5DC666A9A403010C610013CCDC5,
	U3CVibrateAnimU3Ed__33_System_Collections_IEnumerator_Reset_m88E0A911C29AA03E40B41362A615AC65E8D4798A,
	U3CVibrateAnimU3Ed__33_System_Collections_IEnumerator_get_Current_mBD8DF7B95211DD97103BFB324079B71C24AEDE8D,
	GameController_Start_mCC3F0292799528323E2217A12DB08D98CDD492AE,
	GameController_Update_mA76A2CE1F2AC1AACCDBF913CA6E1EA73DC621CD0,
	GameController_FillWithEmptyCells_mB48BDD3791108F6107BF9F8700EF94D034AE90EF,
	GameController_FillInEmptyCells_mAAC291E6668B3D54CC37DCB1EE9B368D793096D3,
	GameController_PlaceBombsAtCells_m59E386DFFD30EC4E86BD51BD725452EF4DD9D407,
	GameController_CalculateNumbersOnCells_mE277990E457A0D9444ADCEAB22F297D5B7FA6718,
	GameController_ClearField_mE9971555CA72D52E8ACD2FDB7E60DB7263A9BDFA,
	GameController_FlipAllCellsWithBombs_m4202509D2C61906811F15E82256698A07BB4CC61,
	GameController_SetAllCellsActivityState_mC3537CC4BD1207FD2A2BDBC9B2442A0A25AD494C,
	GameController_GetNeighbourCells_m28277D2E82B4A26521D4FD6B03727C451D3EC1DB,
	GameController_FlipUnflippedCells_mAED35070EB6A6DC4C5B049A4413FD3DFDC8B26A0,
	GameController_UnmarkMarkedCells_m3D355AC420234321AA8BAAEB30D34F876F48B939,
	GameController_StartNewGame_mD49E9790ABE830D71AD16E830070AD05FFF3E6BC,
	GameController_Restart_m4AEFD85E727195AC4517C6FBE488E42A584527B4,
	GameController_AddCountMarkedCells_mECEB80820F153E578871851C3F10512E337E7CA5,
	GameController_ProcessWinEvent_mF1CCF90F21440B39896B28A1A9264617469C2CD0,
	GameController_CreateTableInConsole_mE4D6A77243C1D826DB8813E715827A14168F72FB,
	GameController__ctor_m9D952052C0A7234373FA5531292FCA8855BE2643,
};
static const int32_t s_InvokerIndices[56] = 
{
	1100,
	1100,
	1100,
	1100,
	1100,
	1100,
	1100,
	1100,
	1100,
	1100,
	1100,
	835,
	835,
	960,
	962,
	415,
	569,
	323,
	463,
	960,
	960,
	960,
	960,
	1100,
	1100,
	1100,
	935,
	1100,
	1088,
	1069,
	1100,
	1069,
	935,
	1100,
	1088,
	1069,
	1100,
	1069,
	1100,
	1100,
	1100,
	569,
	569,
	1100,
	1100,
	1100,
	960,
	463,
	960,
	1100,
	1100,
	1100,
	935,
	1100,
	1100,
	1100,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	56,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
