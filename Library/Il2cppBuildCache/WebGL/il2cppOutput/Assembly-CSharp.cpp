﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Collections.Generic.List`1<CellScript>
struct List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t815A476B0A21E183042059E705F9E505478CD8AE;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// CellScript[]
struct CellScriptU5BU5D_tB562513EB557156A2D2D1DF04A4B99447E93A85F;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// CellScript[0...,0...]
struct CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// CellScript
struct CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// GameController
struct GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D;
// CellScript/<FlipAnim>d__32
struct U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC;
// CellScript/<VibrateAnim>d__33
struct U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;

IL2CPP_EXTERN_C RuntimeClass* CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD;
IL2CPP_EXTERN_C String_t* _stringLiteral174469F0DC1D2C012CBA679BC2295ABD0834AFB7;
IL2CPP_EXTERN_C String_t* _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745;
IL2CPP_EXTERN_C String_t* _stringLiteral46B78FC21D3932AA58CC88ED781E7FE2D3AB98E8;
IL2CPP_EXTERN_C String_t* _stringLiteral78E0EB8CE240AD8239D26F174DBEFF00C4529892;
IL2CPP_EXTERN_C String_t* _stringLiteral838E394589B5693706685699154545855BEAE0B2;
IL2CPP_EXTERN_C String_t* _stringLiteral8C65C0AE1EF18E9EA06D11A2B8151151F9490A0C;
IL2CPP_EXTERN_C String_t* _stringLiteral8CC2154BA99A342FCE979C29AE9C29FFDC415950;
IL2CPP_EXTERN_C String_t* _stringLiteralAE6EFDC0C09F841BA88F172CEE94A07B1705B0AF;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralE8391A0839948AFB6B9EF3EA9ECED133D95DF1B8;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisCellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA_mB5D3ED8FF21204C96C68B4F221507BBC7E46878C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mDBFD9180E77276180C3BB51B5F7A9AA08867934A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mC414BFFA6AB5DA6199FCADA47C75B1AC1641D027_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CFlipAnimU3Ed__32_System_Collections_IEnumerator_Reset_m9346BED6567910CA6F876F1A961BB0EFB5B15431_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CVibrateAnimU3Ed__33_System_Collections_IEnumerator_Reset_m88E0A911C29AA03E40B41362A615AC65E8D4798A_RuntimeMethod_var;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
struct SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77;
struct CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// System.Object


// System.Collections.Generic.List`1<CellScript>
struct List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	CellScriptU5BU5D_tB562513EB557156A2D2D1DF04A4B99447E93A85F* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4, ____items_1)); }
	inline CellScriptU5BU5D_tB562513EB557156A2D2D1DF04A4B99447E93A85F* get__items_1() const { return ____items_1; }
	inline CellScriptU5BU5D_tB562513EB557156A2D2D1DF04A4B99447E93A85F** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(CellScriptU5BU5D_tB562513EB557156A2D2D1DF04A4B99447E93A85F* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	CellScriptU5BU5D_tB562513EB557156A2D2D1DF04A4B99447E93A85F* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4_StaticFields, ____emptyArray_5)); }
	inline CellScriptU5BU5D_tB562513EB557156A2D2D1DF04A4B99447E93A85F* get__emptyArray_5() const { return ____emptyArray_5; }
	inline CellScriptU5BU5D_tB562513EB557156A2D2D1DF04A4B99447E93A85F** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(CellScriptU5BU5D_tB562513EB557156A2D2D1DF04A4B99447E93A85F* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// CellScript/<FlipAnim>d__32
struct U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC  : public RuntimeObject
{
public:
	// System.Int32 CellScript/<FlipAnim>d__32::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object CellScript/<FlipAnim>d__32::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single CellScript/<FlipAnim>d__32::endRotation
	float ___endRotation_2;
	// System.Single CellScript/<FlipAnim>d__32::startRotation
	float ___startRotation_3;
	// System.Int32 CellScript/<FlipAnim>d__32::countFrames
	int32_t ___countFrames_4;
	// CellScript CellScript/<FlipAnim>d__32::<>4__this
	CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * ___U3CU3E4__this_5;
	// System.Single CellScript/<FlipAnim>d__32::<deltaRot>5__2
	float ___U3CdeltaRotU3E5__2_6;
	// System.Int32 CellScript/<FlipAnim>d__32::<i>5__3
	int32_t ___U3CiU3E5__3_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_endRotation_2() { return static_cast<int32_t>(offsetof(U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC, ___endRotation_2)); }
	inline float get_endRotation_2() const { return ___endRotation_2; }
	inline float* get_address_of_endRotation_2() { return &___endRotation_2; }
	inline void set_endRotation_2(float value)
	{
		___endRotation_2 = value;
	}

	inline static int32_t get_offset_of_startRotation_3() { return static_cast<int32_t>(offsetof(U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC, ___startRotation_3)); }
	inline float get_startRotation_3() const { return ___startRotation_3; }
	inline float* get_address_of_startRotation_3() { return &___startRotation_3; }
	inline void set_startRotation_3(float value)
	{
		___startRotation_3 = value;
	}

	inline static int32_t get_offset_of_countFrames_4() { return static_cast<int32_t>(offsetof(U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC, ___countFrames_4)); }
	inline int32_t get_countFrames_4() const { return ___countFrames_4; }
	inline int32_t* get_address_of_countFrames_4() { return &___countFrames_4; }
	inline void set_countFrames_4(int32_t value)
	{
		___countFrames_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC, ___U3CU3E4__this_5)); }
	inline CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdeltaRotU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC, ___U3CdeltaRotU3E5__2_6)); }
	inline float get_U3CdeltaRotU3E5__2_6() const { return ___U3CdeltaRotU3E5__2_6; }
	inline float* get_address_of_U3CdeltaRotU3E5__2_6() { return &___U3CdeltaRotU3E5__2_6; }
	inline void set_U3CdeltaRotU3E5__2_6(float value)
	{
		___U3CdeltaRotU3E5__2_6 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__3_7() { return static_cast<int32_t>(offsetof(U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC, ___U3CiU3E5__3_7)); }
	inline int32_t get_U3CiU3E5__3_7() const { return ___U3CiU3E5__3_7; }
	inline int32_t* get_address_of_U3CiU3E5__3_7() { return &___U3CiU3E5__3_7; }
	inline void set_U3CiU3E5__3_7(int32_t value)
	{
		___U3CiU3E5__3_7 = value;
	}
};


// CellScript/<VibrateAnim>d__33
struct U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE  : public RuntimeObject
{
public:
	// System.Int32 CellScript/<VibrateAnim>d__33::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object CellScript/<VibrateAnim>d__33::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// CellScript CellScript/<VibrateAnim>d__33::<>4__this
	CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * ___U3CU3E4__this_2;
	// System.Int32 CellScript/<VibrateAnim>d__33::deltaFrames
	int32_t ___deltaFrames_3;
	// System.Int32 CellScript/<VibrateAnim>d__33::countFrames
	int32_t ___countFrames_4;
	// System.Single CellScript/<VibrateAnim>d__33::<deltaScale>5__2
	float ___U3CdeltaScaleU3E5__2_5;
	// System.Int32 CellScript/<VibrateAnim>d__33::<i>5__3
	int32_t ___U3CiU3E5__3_6;
	// System.Int32 CellScript/<VibrateAnim>d__33::<j>5__4
	int32_t ___U3CjU3E5__4_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE, ___U3CU3E4__this_2)); }
	inline CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_deltaFrames_3() { return static_cast<int32_t>(offsetof(U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE, ___deltaFrames_3)); }
	inline int32_t get_deltaFrames_3() const { return ___deltaFrames_3; }
	inline int32_t* get_address_of_deltaFrames_3() { return &___deltaFrames_3; }
	inline void set_deltaFrames_3(int32_t value)
	{
		___deltaFrames_3 = value;
	}

	inline static int32_t get_offset_of_countFrames_4() { return static_cast<int32_t>(offsetof(U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE, ___countFrames_4)); }
	inline int32_t get_countFrames_4() const { return ___countFrames_4; }
	inline int32_t* get_address_of_countFrames_4() { return &___countFrames_4; }
	inline void set_countFrames_4(int32_t value)
	{
		___countFrames_4 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaScaleU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE, ___U3CdeltaScaleU3E5__2_5)); }
	inline float get_U3CdeltaScaleU3E5__2_5() const { return ___U3CdeltaScaleU3E5__2_5; }
	inline float* get_address_of_U3CdeltaScaleU3E5__2_5() { return &___U3CdeltaScaleU3E5__2_5; }
	inline void set_U3CdeltaScaleU3E5__2_5(float value)
	{
		___U3CdeltaScaleU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE, ___U3CiU3E5__3_6)); }
	inline int32_t get_U3CiU3E5__3_6() const { return ___U3CiU3E5__3_6; }
	inline int32_t* get_address_of_U3CiU3E5__3_6() { return &___U3CiU3E5__3_6; }
	inline void set_U3CiU3E5__3_6(int32_t value)
	{
		___U3CiU3E5__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CjU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE, ___U3CjU3E5__4_7)); }
	inline int32_t get_U3CjU3E5__4_7() const { return ___U3CjU3E5__4_7; }
	inline int32_t* get_address_of_U3CjU3E5__4_7() { return &___U3CjU3E5__4_7; }
	inline void set_U3CjU3E5__4_7(int32_t value)
	{
		___U3CjU3E5__4_7 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:

public:
};


// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.UI.Image/FillMethod
struct FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/Type
struct Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};

struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// CellScript
struct CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// GameController CellScript::gc
	GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * ___gc_4;
	// UnityEngine.UI.Image CellScript::FrontImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___FrontImage_5;
	// UnityEngine.UI.Image CellScript::BackImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___BackImage_6;
	// UnityEngine.Sprite CellScript::FlagImage
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___FlagImage_7;
	// System.Int32 CellScript::countFramesToFlip
	int32_t ___countFramesToFlip_8;
	// System.Int32 CellScript::countFramesToVibrate
	int32_t ___countFramesToVibrate_9;
	// System.Int32 CellScript::countFramesToSemiVibrate
	int32_t ___countFramesToSemiVibrate_10;
	// System.Int32 CellScript::i
	int32_t ___i_11;
	// System.Int32 CellScript::j
	int32_t ___j_12;
	// System.Int32 CellScript::CountNearBombs
	int32_t ___CountNearBombs_13;
	// System.Boolean CellScript::IsBomb
	bool ___IsBomb_14;
	// System.Boolean CellScript::IsMarked
	bool ___IsMarked_15;
	// System.Boolean CellScript::IsFlippedToBack
	bool ___IsFlippedToBack_16;
	// System.Boolean CellScript::IsActive
	bool ___IsActive_17;
	// System.Boolean CellScript::needToStartRestart
	bool ___needToStartRestart_18;

public:
	inline static int32_t get_offset_of_gc_4() { return static_cast<int32_t>(offsetof(CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA, ___gc_4)); }
	inline GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * get_gc_4() const { return ___gc_4; }
	inline GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 ** get_address_of_gc_4() { return &___gc_4; }
	inline void set_gc_4(GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * value)
	{
		___gc_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gc_4), (void*)value);
	}

	inline static int32_t get_offset_of_FrontImage_5() { return static_cast<int32_t>(offsetof(CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA, ___FrontImage_5)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_FrontImage_5() const { return ___FrontImage_5; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_FrontImage_5() { return &___FrontImage_5; }
	inline void set_FrontImage_5(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___FrontImage_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FrontImage_5), (void*)value);
	}

	inline static int32_t get_offset_of_BackImage_6() { return static_cast<int32_t>(offsetof(CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA, ___BackImage_6)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_BackImage_6() const { return ___BackImage_6; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_BackImage_6() { return &___BackImage_6; }
	inline void set_BackImage_6(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___BackImage_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BackImage_6), (void*)value);
	}

	inline static int32_t get_offset_of_FlagImage_7() { return static_cast<int32_t>(offsetof(CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA, ___FlagImage_7)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_FlagImage_7() const { return ___FlagImage_7; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_FlagImage_7() { return &___FlagImage_7; }
	inline void set_FlagImage_7(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___FlagImage_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FlagImage_7), (void*)value);
	}

	inline static int32_t get_offset_of_countFramesToFlip_8() { return static_cast<int32_t>(offsetof(CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA, ___countFramesToFlip_8)); }
	inline int32_t get_countFramesToFlip_8() const { return ___countFramesToFlip_8; }
	inline int32_t* get_address_of_countFramesToFlip_8() { return &___countFramesToFlip_8; }
	inline void set_countFramesToFlip_8(int32_t value)
	{
		___countFramesToFlip_8 = value;
	}

	inline static int32_t get_offset_of_countFramesToVibrate_9() { return static_cast<int32_t>(offsetof(CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA, ___countFramesToVibrate_9)); }
	inline int32_t get_countFramesToVibrate_9() const { return ___countFramesToVibrate_9; }
	inline int32_t* get_address_of_countFramesToVibrate_9() { return &___countFramesToVibrate_9; }
	inline void set_countFramesToVibrate_9(int32_t value)
	{
		___countFramesToVibrate_9 = value;
	}

	inline static int32_t get_offset_of_countFramesToSemiVibrate_10() { return static_cast<int32_t>(offsetof(CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA, ___countFramesToSemiVibrate_10)); }
	inline int32_t get_countFramesToSemiVibrate_10() const { return ___countFramesToSemiVibrate_10; }
	inline int32_t* get_address_of_countFramesToSemiVibrate_10() { return &___countFramesToSemiVibrate_10; }
	inline void set_countFramesToSemiVibrate_10(int32_t value)
	{
		___countFramesToSemiVibrate_10 = value;
	}

	inline static int32_t get_offset_of_i_11() { return static_cast<int32_t>(offsetof(CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA, ___i_11)); }
	inline int32_t get_i_11() const { return ___i_11; }
	inline int32_t* get_address_of_i_11() { return &___i_11; }
	inline void set_i_11(int32_t value)
	{
		___i_11 = value;
	}

	inline static int32_t get_offset_of_j_12() { return static_cast<int32_t>(offsetof(CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA, ___j_12)); }
	inline int32_t get_j_12() const { return ___j_12; }
	inline int32_t* get_address_of_j_12() { return &___j_12; }
	inline void set_j_12(int32_t value)
	{
		___j_12 = value;
	}

	inline static int32_t get_offset_of_CountNearBombs_13() { return static_cast<int32_t>(offsetof(CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA, ___CountNearBombs_13)); }
	inline int32_t get_CountNearBombs_13() const { return ___CountNearBombs_13; }
	inline int32_t* get_address_of_CountNearBombs_13() { return &___CountNearBombs_13; }
	inline void set_CountNearBombs_13(int32_t value)
	{
		___CountNearBombs_13 = value;
	}

	inline static int32_t get_offset_of_IsBomb_14() { return static_cast<int32_t>(offsetof(CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA, ___IsBomb_14)); }
	inline bool get_IsBomb_14() const { return ___IsBomb_14; }
	inline bool* get_address_of_IsBomb_14() { return &___IsBomb_14; }
	inline void set_IsBomb_14(bool value)
	{
		___IsBomb_14 = value;
	}

	inline static int32_t get_offset_of_IsMarked_15() { return static_cast<int32_t>(offsetof(CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA, ___IsMarked_15)); }
	inline bool get_IsMarked_15() const { return ___IsMarked_15; }
	inline bool* get_address_of_IsMarked_15() { return &___IsMarked_15; }
	inline void set_IsMarked_15(bool value)
	{
		___IsMarked_15 = value;
	}

	inline static int32_t get_offset_of_IsFlippedToBack_16() { return static_cast<int32_t>(offsetof(CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA, ___IsFlippedToBack_16)); }
	inline bool get_IsFlippedToBack_16() const { return ___IsFlippedToBack_16; }
	inline bool* get_address_of_IsFlippedToBack_16() { return &___IsFlippedToBack_16; }
	inline void set_IsFlippedToBack_16(bool value)
	{
		___IsFlippedToBack_16 = value;
	}

	inline static int32_t get_offset_of_IsActive_17() { return static_cast<int32_t>(offsetof(CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA, ___IsActive_17)); }
	inline bool get_IsActive_17() const { return ___IsActive_17; }
	inline bool* get_address_of_IsActive_17() { return &___IsActive_17; }
	inline void set_IsActive_17(bool value)
	{
		___IsActive_17 = value;
	}

	inline static int32_t get_offset_of_needToStartRestart_18() { return static_cast<int32_t>(offsetof(CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA, ___needToStartRestart_18)); }
	inline bool get_needToStartRestart_18() const { return ___needToStartRestart_18; }
	inline bool* get_address_of_needToStartRestart_18() { return &___needToStartRestart_18; }
	inline void set_needToStartRestart_18(bool value)
	{
		___needToStartRestart_18 = value;
	}
};


// GameController
struct GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Camera GameController::Camera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___Camera_4;
	// UnityEngine.Transform GameController::CellsContainer
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___CellsContainer_5;
	// UnityEngine.GameObject GameController::CellPrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___CellPrefab_6;
	// System.Int32 GameController::CountCellsInLine
	int32_t ___CountCellsInLine_7;
	// System.Int32 GameController::CountCellsInColumn
	int32_t ___CountCellsInColumn_8;
	// System.Single GameController::DistanceBetweenCells
	float ___DistanceBetweenCells_9;
	// System.Int32 GameController::CountBombs
	int32_t ___CountBombs_10;
	// System.Single GameController::AfterVibrationsRestartTime
	float ___AfterVibrationsRestartTime_11;
	// System.Single GameController::AutoRestartTime
	float ___AutoRestartTime_12;
	// UnityEngine.Sprite[] GameController::tiles
	SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* ___tiles_13;
	// System.Boolean GameController::IsBombsPlaced
	bool ___IsBombsPlaced_14;
	// System.Int32 GameController::countUnbombedCells
	int32_t ___countUnbombedCells_15;
	// System.Int32 GameController::countMarkedCells
	int32_t ___countMarkedCells_16;
	// CellScript[0...,0...] GameController::cellScripts
	CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* ___cellScripts_17;
	// System.Int32 GameController::countCells
	int32_t ___countCells_18;

public:
	inline static int32_t get_offset_of_Camera_4() { return static_cast<int32_t>(offsetof(GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274, ___Camera_4)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_Camera_4() const { return ___Camera_4; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_Camera_4() { return &___Camera_4; }
	inline void set_Camera_4(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___Camera_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Camera_4), (void*)value);
	}

	inline static int32_t get_offset_of_CellsContainer_5() { return static_cast<int32_t>(offsetof(GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274, ___CellsContainer_5)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_CellsContainer_5() const { return ___CellsContainer_5; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_CellsContainer_5() { return &___CellsContainer_5; }
	inline void set_CellsContainer_5(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___CellsContainer_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CellsContainer_5), (void*)value);
	}

	inline static int32_t get_offset_of_CellPrefab_6() { return static_cast<int32_t>(offsetof(GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274, ___CellPrefab_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_CellPrefab_6() const { return ___CellPrefab_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_CellPrefab_6() { return &___CellPrefab_6; }
	inline void set_CellPrefab_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___CellPrefab_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CellPrefab_6), (void*)value);
	}

	inline static int32_t get_offset_of_CountCellsInLine_7() { return static_cast<int32_t>(offsetof(GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274, ___CountCellsInLine_7)); }
	inline int32_t get_CountCellsInLine_7() const { return ___CountCellsInLine_7; }
	inline int32_t* get_address_of_CountCellsInLine_7() { return &___CountCellsInLine_7; }
	inline void set_CountCellsInLine_7(int32_t value)
	{
		___CountCellsInLine_7 = value;
	}

	inline static int32_t get_offset_of_CountCellsInColumn_8() { return static_cast<int32_t>(offsetof(GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274, ___CountCellsInColumn_8)); }
	inline int32_t get_CountCellsInColumn_8() const { return ___CountCellsInColumn_8; }
	inline int32_t* get_address_of_CountCellsInColumn_8() { return &___CountCellsInColumn_8; }
	inline void set_CountCellsInColumn_8(int32_t value)
	{
		___CountCellsInColumn_8 = value;
	}

	inline static int32_t get_offset_of_DistanceBetweenCells_9() { return static_cast<int32_t>(offsetof(GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274, ___DistanceBetweenCells_9)); }
	inline float get_DistanceBetweenCells_9() const { return ___DistanceBetweenCells_9; }
	inline float* get_address_of_DistanceBetweenCells_9() { return &___DistanceBetweenCells_9; }
	inline void set_DistanceBetweenCells_9(float value)
	{
		___DistanceBetweenCells_9 = value;
	}

	inline static int32_t get_offset_of_CountBombs_10() { return static_cast<int32_t>(offsetof(GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274, ___CountBombs_10)); }
	inline int32_t get_CountBombs_10() const { return ___CountBombs_10; }
	inline int32_t* get_address_of_CountBombs_10() { return &___CountBombs_10; }
	inline void set_CountBombs_10(int32_t value)
	{
		___CountBombs_10 = value;
	}

	inline static int32_t get_offset_of_AfterVibrationsRestartTime_11() { return static_cast<int32_t>(offsetof(GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274, ___AfterVibrationsRestartTime_11)); }
	inline float get_AfterVibrationsRestartTime_11() const { return ___AfterVibrationsRestartTime_11; }
	inline float* get_address_of_AfterVibrationsRestartTime_11() { return &___AfterVibrationsRestartTime_11; }
	inline void set_AfterVibrationsRestartTime_11(float value)
	{
		___AfterVibrationsRestartTime_11 = value;
	}

	inline static int32_t get_offset_of_AutoRestartTime_12() { return static_cast<int32_t>(offsetof(GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274, ___AutoRestartTime_12)); }
	inline float get_AutoRestartTime_12() const { return ___AutoRestartTime_12; }
	inline float* get_address_of_AutoRestartTime_12() { return &___AutoRestartTime_12; }
	inline void set_AutoRestartTime_12(float value)
	{
		___AutoRestartTime_12 = value;
	}

	inline static int32_t get_offset_of_tiles_13() { return static_cast<int32_t>(offsetof(GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274, ___tiles_13)); }
	inline SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* get_tiles_13() const { return ___tiles_13; }
	inline SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77** get_address_of_tiles_13() { return &___tiles_13; }
	inline void set_tiles_13(SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* value)
	{
		___tiles_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tiles_13), (void*)value);
	}

	inline static int32_t get_offset_of_IsBombsPlaced_14() { return static_cast<int32_t>(offsetof(GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274, ___IsBombsPlaced_14)); }
	inline bool get_IsBombsPlaced_14() const { return ___IsBombsPlaced_14; }
	inline bool* get_address_of_IsBombsPlaced_14() { return &___IsBombsPlaced_14; }
	inline void set_IsBombsPlaced_14(bool value)
	{
		___IsBombsPlaced_14 = value;
	}

	inline static int32_t get_offset_of_countUnbombedCells_15() { return static_cast<int32_t>(offsetof(GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274, ___countUnbombedCells_15)); }
	inline int32_t get_countUnbombedCells_15() const { return ___countUnbombedCells_15; }
	inline int32_t* get_address_of_countUnbombedCells_15() { return &___countUnbombedCells_15; }
	inline void set_countUnbombedCells_15(int32_t value)
	{
		___countUnbombedCells_15 = value;
	}

	inline static int32_t get_offset_of_countMarkedCells_16() { return static_cast<int32_t>(offsetof(GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274, ___countMarkedCells_16)); }
	inline int32_t get_countMarkedCells_16() const { return ___countMarkedCells_16; }
	inline int32_t* get_address_of_countMarkedCells_16() { return &___countMarkedCells_16; }
	inline void set_countMarkedCells_16(int32_t value)
	{
		___countMarkedCells_16 = value;
	}

	inline static int32_t get_offset_of_cellScripts_17() { return static_cast<int32_t>(offsetof(GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274, ___cellScripts_17)); }
	inline CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* get_cellScripts_17() const { return ___cellScripts_17; }
	inline CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5** get_address_of_cellScripts_17() { return &___cellScripts_17; }
	inline void set_cellScripts_17(CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* value)
	{
		___cellScripts_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cellScripts_17), (void*)value);
	}

	inline static int32_t get_offset_of_countCells_18() { return static_cast<int32_t>(offsetof(GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274, ___countCells_18)); }
	inline int32_t get_countCells_18() const { return ___countCells_18; }
	inline int32_t* get_address_of_countCells_18() { return &___countCells_18; }
	inline void set_countCells_18(int32_t value)
	{
		___countCells_18 = value;
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_Sprite_37;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_OverrideSprite_38;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_39;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_40;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_41;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_42;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_43;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_44;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_45;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_46;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_47;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_48;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_49;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_50;

public:
	inline static int32_t get_offset_of_m_Sprite_37() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Sprite_37)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_Sprite_37() const { return ___m_Sprite_37; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_Sprite_37() { return &___m_Sprite_37; }
	inline void set_m_Sprite_37(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_Sprite_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sprite_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_38() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_OverrideSprite_38)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_OverrideSprite_38() const { return ___m_OverrideSprite_38; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_OverrideSprite_38() { return &___m_OverrideSprite_38; }
	inline void set_m_OverrideSprite_38(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_OverrideSprite_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideSprite_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_39() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Type_39)); }
	inline int32_t get_m_Type_39() const { return ___m_Type_39; }
	inline int32_t* get_address_of_m_Type_39() { return &___m_Type_39; }
	inline void set_m_Type_39(int32_t value)
	{
		___m_Type_39 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_40() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PreserveAspect_40)); }
	inline bool get_m_PreserveAspect_40() const { return ___m_PreserveAspect_40; }
	inline bool* get_address_of_m_PreserveAspect_40() { return &___m_PreserveAspect_40; }
	inline void set_m_PreserveAspect_40(bool value)
	{
		___m_PreserveAspect_40 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_41() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillCenter_41)); }
	inline bool get_m_FillCenter_41() const { return ___m_FillCenter_41; }
	inline bool* get_address_of_m_FillCenter_41() { return &___m_FillCenter_41; }
	inline void set_m_FillCenter_41(bool value)
	{
		___m_FillCenter_41 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_42() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillMethod_42)); }
	inline int32_t get_m_FillMethod_42() const { return ___m_FillMethod_42; }
	inline int32_t* get_address_of_m_FillMethod_42() { return &___m_FillMethod_42; }
	inline void set_m_FillMethod_42(int32_t value)
	{
		___m_FillMethod_42 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_43() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillAmount_43)); }
	inline float get_m_FillAmount_43() const { return ___m_FillAmount_43; }
	inline float* get_address_of_m_FillAmount_43() { return &___m_FillAmount_43; }
	inline void set_m_FillAmount_43(float value)
	{
		___m_FillAmount_43 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_44() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillClockwise_44)); }
	inline bool get_m_FillClockwise_44() const { return ___m_FillClockwise_44; }
	inline bool* get_address_of_m_FillClockwise_44() { return &___m_FillClockwise_44; }
	inline void set_m_FillClockwise_44(bool value)
	{
		___m_FillClockwise_44 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_45() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillOrigin_45)); }
	inline int32_t get_m_FillOrigin_45() const { return ___m_FillOrigin_45; }
	inline int32_t* get_address_of_m_FillOrigin_45() { return &___m_FillOrigin_45; }
	inline void set_m_FillOrigin_45(int32_t value)
	{
		___m_FillOrigin_45 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_46() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_AlphaHitTestMinimumThreshold_46)); }
	inline float get_m_AlphaHitTestMinimumThreshold_46() const { return ___m_AlphaHitTestMinimumThreshold_46; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_46() { return &___m_AlphaHitTestMinimumThreshold_46; }
	inline void set_m_AlphaHitTestMinimumThreshold_46(float value)
	{
		___m_AlphaHitTestMinimumThreshold_46 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_47() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Tracked_47)); }
	inline bool get_m_Tracked_47() const { return ___m_Tracked_47; }
	inline bool* get_address_of_m_Tracked_47() { return &___m_Tracked_47; }
	inline void set_m_Tracked_47(bool value)
	{
		___m_Tracked_47 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_48() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_UseSpriteMesh_48)); }
	inline bool get_m_UseSpriteMesh_48() const { return ___m_UseSpriteMesh_48; }
	inline bool* get_address_of_m_UseSpriteMesh_48() { return &___m_UseSpriteMesh_48; }
	inline void set_m_UseSpriteMesh_48(bool value)
	{
		___m_UseSpriteMesh_48 = value;
	}

	inline static int32_t get_offset_of_m_PixelsPerUnitMultiplier_49() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PixelsPerUnitMultiplier_49)); }
	inline float get_m_PixelsPerUnitMultiplier_49() const { return ___m_PixelsPerUnitMultiplier_49; }
	inline float* get_address_of_m_PixelsPerUnitMultiplier_49() { return &___m_PixelsPerUnitMultiplier_49; }
	inline void set_m_PixelsPerUnitMultiplier_49(float value)
	{
		___m_PixelsPerUnitMultiplier_49 = value;
	}

	inline static int32_t get_offset_of_m_CachedReferencePixelsPerUnit_50() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_CachedReferencePixelsPerUnit_50)); }
	inline float get_m_CachedReferencePixelsPerUnit_50() const { return ___m_CachedReferencePixelsPerUnit_50; }
	inline float* get_address_of_m_CachedReferencePixelsPerUnit_50() { return &___m_CachedReferencePixelsPerUnit_50; }
	inline void set_m_CachedReferencePixelsPerUnit_50(float value)
	{
		___m_CachedReferencePixelsPerUnit_50 = value;
	}
};

struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_ETC1DefaultUI_36;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_VertScratch_51;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_UVScratch_52;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Xy_53;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Uv_54;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t815A476B0A21E183042059E705F9E505478CD8AE * ___m_TrackedTexturelessImages_55;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_56;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_36() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_ETC1DefaultUI_36)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_ETC1DefaultUI_36() const { return ___s_ETC1DefaultUI_36; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_ETC1DefaultUI_36() { return &___s_ETC1DefaultUI_36; }
	inline void set_s_ETC1DefaultUI_36(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_ETC1DefaultUI_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ETC1DefaultUI_36), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertScratch_51() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_VertScratch_51)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_VertScratch_51() const { return ___s_VertScratch_51; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_VertScratch_51() { return &___s_VertScratch_51; }
	inline void set_s_VertScratch_51(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_VertScratch_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertScratch_51), (void*)value);
	}

	inline static int32_t get_offset_of_s_UVScratch_52() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_UVScratch_52)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_UVScratch_52() const { return ___s_UVScratch_52; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_UVScratch_52() { return &___s_UVScratch_52; }
	inline void set_s_UVScratch_52(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_UVScratch_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UVScratch_52), (void*)value);
	}

	inline static int32_t get_offset_of_s_Xy_53() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Xy_53)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Xy_53() const { return ___s_Xy_53; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Xy_53() { return &___s_Xy_53; }
	inline void set_s_Xy_53(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Xy_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Xy_53), (void*)value);
	}

	inline static int32_t get_offset_of_s_Uv_54() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Uv_54)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Uv_54() const { return ___s_Uv_54; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Uv_54() { return &___s_Uv_54; }
	inline void set_s_Uv_54(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Uv_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Uv_54), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_55() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___m_TrackedTexturelessImages_55)); }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE * get_m_TrackedTexturelessImages_55() const { return ___m_TrackedTexturelessImages_55; }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE ** get_address_of_m_TrackedTexturelessImages_55() { return &___m_TrackedTexturelessImages_55; }
	inline void set_m_TrackedTexturelessImages_55(List_1_t815A476B0A21E183042059E705F9E505478CD8AE * value)
	{
		___m_TrackedTexturelessImages_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedTexturelessImages_55), (void*)value);
	}

	inline static int32_t get_offset_of_s_Initialized_56() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Initialized_56)); }
	inline bool get_s_Initialized_56() const { return ___s_Initialized_56; }
	inline bool* get_address_of_s_Initialized_56() { return &___s_Initialized_56; }
	inline void set_s_Initialized_56(bool value)
	{
		___s_Initialized_56 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * m_Items[1];

public:
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// CellScript[0...,0...]
struct CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * m_Items[1];

public:
	inline CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA ** GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA ** GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * value)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_mB05DEC51C29EF5BB8BD17D055E80217F11E571AA_gshared (RuntimeObject * ___original0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position1, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation2, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);

// System.Void CellScript::SetCellScale(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_SetCellScale_mE30F34C461AF6953FEAE12111E0690040C8EF5C0 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, float ___scaleMultiplier0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetMouseButtonDown_m466D81FDCC87C9CB07557B39DCB435EB691F1EF3 (int32_t ___button0, const RuntimeMethod* method);
// System.Void CellScript::ProcessLMB()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_ProcessLMB_m481D20CA4888088580C146C5DDFC96BEA07546E8 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, const RuntimeMethod* method);
// System.Void CellScript::ProcessRMB()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_ProcessRMB_m7D698686318DDAF79BC482CBBAC1464D13235996 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, const RuntimeMethod* method);
// System.Void CellScript::ProcessMMB()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_ProcessMMB_mC2AF2AA51CBD7B3E015418DA0F8223DD3318669A (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, const RuntimeMethod* method);
// System.Void CellScript::OpenNearCells()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_OpenNearCells_m8D67AD843269C6D9711A83FE6B2900FFF42A8617 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, const RuntimeMethod* method);
// System.Void CellScript::OpenCell()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_OpenCell_m6D08D2BA5A1BBA51FCC9CF866349AD92FE3850BC (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, const RuntimeMethod* method);
// System.Void CellScript::MarkCell()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_MarkCell_m1CEB7D6C1528F8812859B33F66E3AF876970B361 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, const RuntimeMethod* method);
// System.Void GameController::FillInEmptyCells(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_FillInEmptyCells_mAAC291E6668B3D54CC37DCB1EE9B368D793096D3 (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, int32_t ___col0, int32_t ___lin1, const RuntimeMethod* method);
// System.Void CellScript::SetActivityState(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_SetActivityState_mB96C73DD2424B3A37A0BCDFA8E93AE3B26681C18 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, bool ___newState0, const RuntimeMethod* method);
// System.Void CellScript::Lose_FirstStage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_Lose_FirstStage_m3556D5B3504B23B58F53C76852DAA56010F540A7 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, const RuntimeMethod* method);
// System.Void GameController::AddCountMarkedCells(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_AddCountMarkedCells_mECEB80820F153E578871851C3F10512E337E7CA5 (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, int32_t ___addictiveNum0, const RuntimeMethod* method);
// System.Void CellScript::Flip(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_Flip_m2FDF12D98C578A46DA54906E33BCE6F47AA2A702 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, bool ___toBackSide0, const RuntimeMethod* method);
// System.Void CellScript::setMarkedState(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CellScript_setMarkedState_m464FBE7E2E19E707F61160CAF5D92FCFB82001A0_inline (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, bool ___newState0, const RuntimeMethod* method);
// System.Collections.Generic.List`1<CellScript> GameController::GetNeighbourCells(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * GameController_GetNeighbourCells_m28277D2E82B4A26521D4FD6B03727C451D3EC1DB (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, int32_t ___i0, int32_t ___j1, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<CellScript>::get_Item(System.Int32)
inline CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_inline (List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * (*) (List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Int32 System.Collections.Generic.List`1<CellScript>::get_Count()
inline int32_t List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_inline (List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4 (Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * __this, Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___value0, const RuntimeMethod* method);
// System.Boolean CellScript::IsEqualsBombsAndMarkersCount(System.Collections.Generic.List`1<CellScript>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CellScript_IsEqualsBombsAndMarkersCount_mCE82439D14A55EA840FEF814C5DC2A4763C83DEC (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * ___nearCells0, const RuntimeMethod* method);
// System.Boolean CellScript::IsRightMarking(System.Collections.Generic.List`1<CellScript>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CellScript_IsRightMarking_m54D63C04E0117E412BF3509668D6BB27F66F8DDB (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * ___nearCells0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour_print_m4F113B89EC1C221CAC6EC64365E6DAD0AF86F090 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void CellScript::StartVibrate(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_StartVibrate_m68C587BAA15ECC2C23DF1A398EEFE58ACAC07FD5 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, int32_t ___countFrames0, int32_t ___deltaFrames1, const RuntimeMethod* method);
// System.Void CellScript::StartFlip(System.Single,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_StartFlip_m68EDAC191366E438E78ABFB2301A1810FAD11565 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, float ___startRotation0, float ___endRotation1, int32_t ___countFrames2, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Collections.IEnumerator CellScript::FlipAnim(System.Single,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CellScript_FlipAnim_mF440633B7B3F7B578A1F18481670F9A95E0BE2B6 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, float ___startRotation0, float ___endRotation1, int32_t ___countFrames2, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.Collections.IEnumerator CellScript::VibrateAnim(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CellScript_VibrateAnim_m6A77E2934C12D43DBDDA4AF2B2AAE53FCFE7C415 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, int32_t ___countFrames0, int32_t ___deltaFrames1, const RuntimeMethod* method);
// System.Void CellScript/<FlipAnim>d__32::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CFlipAnimU3Ed__32__ctor_m3C8306BE011D741F286EEF99EF338678674A5B20 (U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void CellScript/<VibrateAnim>d__33::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVibrateAnimU3Ed__33__ctor_m1C4133168D5711366DB5305F2A4FBE525697AE9F (U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void GameController::SetAllCellsActivityState(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_SetAllCellsActivityState_mC3537CC4BD1207FD2A2BDBC9B2442A0A25AD494C (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, bool ___newState0, const RuntimeMethod* method);
// System.Void GameController::FlipAllCellsWithBombs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_FlipAllCellsWithBombs_m4202509D2C61906811F15E82256698A07BB4CC61 (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, const RuntimeMethod* method);
// System.Void GameController::Restart()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_Restart_m4AEFD85E727195AC4517C6FBE488E42A584527B4 (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// System.Void GameController::StartNewGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_StartNewGame_mD49E9790ABE830D71AD16E830070AD05FFF3E6BC (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, int32_t ___index0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Transform::get_childCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Transform_get_childCount_mCBED4F6D3F6A7386C4D97C2C3FD25C383A0BCD05 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702 (const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___original0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position1, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation2, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E , Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_mB05DEC51C29EF5BB8BD17D055E80217F11E571AA_gshared)(___original0, ___position1, ___rotation2, method);
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_parent_mEAE304E1A804E8B83054CEECB5BF1E517196EC13 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___value0, const RuntimeMethod* method);
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411 (int32_t* __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, String_t* ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<CellScript>()
inline CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * GameObject_GetComponent_TisCellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA_mB5D3ED8FF21204C96C68B4F221507BBC7E46878C (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Camera_set_orthographicSize_mFC4BBB0BB0097A5FE13E99D8388DF3008971085F (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * __this, float ___value0, const RuntimeMethod* method);
// System.Void GameController::PlaceBombsAtCells(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_PlaceBombsAtCells_m59E386DFFD30EC4E86BD51BD725452EF4DD9D407 (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, int32_t ___voidCol0, int32_t ___voidLin1, const RuntimeMethod* method);
// System.Void GameController::CalculateNumbersOnCells()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_CalculateNumbersOnCells_mE277990E457A0D9444ADCEAB22F297D5B7FA6718 (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A (int32_t ___minInclusive0, int32_t ___maxExclusive1, const RuntimeMethod* method);
// System.Void CellScript::SetBombState(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CellScript_SetBombState_m34FA5F4F7839C8343B34D2AB0A2729B3EDB95F7C_inline (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, bool ___newState0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<CellScript>::.ctor()
inline void List_1__ctor_mC414BFFA6AB5DA6199FCADA47C75B1AC1641D027 (List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<CellScript>::Add(!0)
inline void List_1_Add_mDBFD9180E77276180C3BB51B5F7A9AA08867934A (List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * __this, CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 *, CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Void GameController::ClearField()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_ClearField_mE9971555CA72D52E8ACD2FDB7E60DB7263A9BDFA (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, const RuntimeMethod* method);
// System.Void GameController::FillWithEmptyCells()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_FillWithEmptyCells_mB48BDD3791108F6107BF9F8700EF94D034AE90EF (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, const RuntimeMethod* method);
// System.Void GameController::UnmarkMarkedCells()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_UnmarkMarkedCells_m3D355AC420234321AA8BAAEB30D34F876F48B939 (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, const RuntimeMethod* method);
// System.Void GameController::FlipUnflippedCells(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_FlipUnflippedCells_mAED35070EB6A6DC4C5B049A4413FD3DFDC8B26A0 (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, bool ___isFlipToBackSide0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour_Invoke_m4AAB759653B1C6FB0653527F4DDC72D1E9162CC4 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, String_t* ___methodName0, float ___time1, const RuntimeMethod* method);
// System.Void GameController::ProcessWinEvent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_ProcessWinEvent_mF1CCF90F21440B39896B28A1A9264617469C2CD0 (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Rotate_mA3AE6D55AA9CC88A8F03C2B0B7CB3DB45ABA6A8E (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, float ___xAngle0, float ___yAngle1, float ___zAngle2, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3 (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___value0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CellScript::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_Start_m2187C5623EDD85CF8C15CFE183C1F10C3D004783 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, const RuntimeMethod* method)
{
	{
		// IsActive = true;
		__this->set_IsActive_17((bool)1);
		// IsFlippedToBack = false;
		__this->set_IsFlippedToBack_16((bool)0);
		// IsMarked = false;
		__this->set_IsMarked_15((bool)0);
		// IsBomb = false;
		__this->set_IsBomb_14((bool)0);
		// needToStartRestart = false;
		__this->set_needToStartRestart_18((bool)0);
		// CountNearBombs = 0;
		__this->set_CountNearBombs_13(0);
		// }
		return;
	}
}
// System.Void CellScript::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_Update_m90DEEF89B07C9B39B2AA05AE208D70CB16898939 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void CellScript::OnMouseEnter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_OnMouseEnter_mF92805A030B64CC9F4330E458D466378DC837C00 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, const RuntimeMethod* method)
{
	{
		// if (IsActive == true)
		bool L_0 = __this->get_IsActive_17();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		// SetCellScale(1.2f);
		CellScript_SetCellScale_mE30F34C461AF6953FEAE12111E0690040C8EF5C0(__this, (1.20000005f), /*hidden argument*/NULL);
	}

IL_0013:
	{
		// }
		return;
	}
}
// System.Void CellScript::OnMouseExit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_OnMouseExit_m43B3758544712677B636125BCBA74610CBB71CF7 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, const RuntimeMethod* method)
{
	{
		// if (IsActive == true)
		bool L_0 = __this->get_IsActive_17();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		// SetCellScale(1f);
		CellScript_SetCellScale_mE30F34C461AF6953FEAE12111E0690040C8EF5C0(__this, (1.0f), /*hidden argument*/NULL);
	}

IL_0013:
	{
		// }
		return;
	}
}
// System.Void CellScript::OnMouseOver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_OnMouseOver_mBA075D3E06B5B30318192F9CB569D9E8B1AE27B5 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, const RuntimeMethod* method)
{
	{
		// if (IsActive)
		bool L_0 = __this->get_IsActive_17();
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		// if (Input.GetMouseButtonDown(0))
		bool L_1;
		L_1 = Input_GetMouseButtonDown_m466D81FDCC87C9CB07557B39DCB435EB691F1EF3(0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		// ProcessLMB();
		CellScript_ProcessLMB_m481D20CA4888088580C146C5DDFC96BEA07546E8(__this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		// if (Input.GetMouseButtonDown(1))
		bool L_2;
		L_2 = Input_GetMouseButtonDown_m466D81FDCC87C9CB07557B39DCB435EB691F1EF3(1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		// ProcessRMB();
		CellScript_ProcessRMB_m7D698686318DDAF79BC482CBBAC1464D13235996(__this, /*hidden argument*/NULL);
	}

IL_0024:
	{
		// if (Input.GetMouseButtonDown(2))
		bool L_3;
		L_3 = Input_GetMouseButtonDown_m466D81FDCC87C9CB07557B39DCB435EB691F1EF3(2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		// ProcessMMB();
		CellScript_ProcessMMB_mC2AF2AA51CBD7B3E015418DA0F8223DD3318669A(__this, /*hidden argument*/NULL);
	}

IL_0032:
	{
		// }
		return;
	}
}
// System.Void CellScript::ProcessLMB()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_ProcessLMB_m481D20CA4888088580C146C5DDFC96BEA07546E8 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, const RuntimeMethod* method)
{
	{
		// if (IsFlippedToBack)
		bool L_0 = __this->get_IsFlippedToBack_16();
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		// OpenNearCells();
		CellScript_OpenNearCells_m8D67AD843269C6D9711A83FE6B2900FFF42A8617(__this, /*hidden argument*/NULL);
		return;
	}

IL_000f:
	{
		// OpenCell();
		CellScript_OpenCell_m6D08D2BA5A1BBA51FCC9CF866349AD92FE3850BC(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CellScript::ProcessRMB()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_ProcessRMB_m7D698686318DDAF79BC482CBBAC1464D13235996 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, const RuntimeMethod* method)
{
	{
		// if (!IsFlippedToBack)
		bool L_0 = __this->get_IsFlippedToBack_16();
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		// MarkCell();
		CellScript_MarkCell_m1CEB7D6C1528F8812859B33F66E3AF876970B361(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		// }
		return;
	}
}
// System.Void CellScript::ProcessMMB()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_ProcessMMB_mC2AF2AA51CBD7B3E015418DA0F8223DD3318669A (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, const RuntimeMethod* method)
{
	{
		// OpenNearCells();
		CellScript_OpenNearCells_m8D67AD843269C6D9711A83FE6B2900FFF42A8617(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CellScript::OpenCell()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_OpenCell_m6D08D2BA5A1BBA51FCC9CF866349AD92FE3850BC (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		// if (!gc.IsBombsPlaced)
		GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * L_0 = __this->get_gc_4();
		bool L_1 = L_0->get_IsBombsPlaced_14();
		if (L_1)
		{
			goto IL_0024;
		}
	}
	{
		// gc.FillInEmptyCells(i, j);
		GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * L_2 = __this->get_gc_4();
		int32_t L_3 = __this->get_i_11();
		int32_t L_4 = __this->get_j_12();
		GameController_FillInEmptyCells_mAAC291E6668B3D54CC37DCB1EE9B368D793096D3(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0024:
	{
		// if (IsBomb == true && IsMarked == false)
		bool L_5 = __this->get_IsBomb_14();
		if (!L_5)
		{
			goto IL_0049;
		}
	}
	{
		bool L_6 = __this->get_IsMarked_15();
		if (L_6)
		{
			goto IL_0049;
		}
	}
	{
		// SetActivityState(false);
		CellScript_SetActivityState_mB96C73DD2424B3A37A0BCDFA8E93AE3B26681C18(__this, (bool)0, /*hidden argument*/NULL);
		// needToStartRestart = true;
		__this->set_needToStartRestart_18((bool)1);
		// Lose_FirstStage();
		CellScript_Lose_FirstStage_m3556D5B3504B23B58F53C76852DAA56010F540A7(__this, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0049:
	{
		// else if (IsFlippedToBack == false && IsMarked == false)
		bool L_7 = __this->get_IsFlippedToBack_16();
		if (L_7)
		{
			goto IL_00c2;
		}
	}
	{
		bool L_8 = __this->get_IsMarked_15();
		if (L_8)
		{
			goto IL_00c2;
		}
	}
	{
		// gc.AddCountMarkedCells();
		GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * L_9 = __this->get_gc_4();
		GameController_AddCountMarkedCells_mECEB80820F153E578871851C3F10512E337E7CA5(L_9, 1, /*hidden argument*/NULL);
		// Flip(true);
		CellScript_Flip_m2FDF12D98C578A46DA54906E33BCE6F47AA2A702(__this, (bool)1, /*hidden argument*/NULL);
		// setMarkedState(false);
		CellScript_setMarkedState_m464FBE7E2E19E707F61160CAF5D92FCFB82001A0_inline(__this, (bool)0, /*hidden argument*/NULL);
		// if (CountNearBombs == 0)
		int32_t L_10 = __this->get_CountNearBombs_13();
		if (L_10)
		{
			goto IL_00c2;
		}
	}
	{
		// SetCellScale(1f);
		CellScript_SetCellScale_mE30F34C461AF6953FEAE12111E0690040C8EF5C0(__this, (1.0f), /*hidden argument*/NULL);
		// SetActivityState(false);
		CellScript_SetActivityState_mB96C73DD2424B3A37A0BCDFA8E93AE3B26681C18(__this, (bool)0, /*hidden argument*/NULL);
		// List<CellScript> nearCells = gc.GetNeighbourCells(i, j);
		GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * L_11 = __this->get_gc_4();
		int32_t L_12 = __this->get_i_11();
		int32_t L_13 = __this->get_j_12();
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_14;
		L_14 = GameController_GetNeighbourCells_m28277D2E82B4A26521D4FD6B03727C451D3EC1DB(L_11, L_12, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		// for (int i = 0; i < nearCells.Count; i++)
		V_1 = 0;
		goto IL_00b9;
	}

IL_00a9:
	{
		// nearCells[i].OpenCell();
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_15 = V_0;
		int32_t L_16 = V_1;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_17;
		L_17 = List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_inline(L_15, L_16, /*hidden argument*/List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_RuntimeMethod_var);
		CellScript_OpenCell_m6D08D2BA5A1BBA51FCC9CF866349AD92FE3850BC(L_17, /*hidden argument*/NULL);
		// for (int i = 0; i < nearCells.Count; i++)
		int32_t L_18 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_00b9:
	{
		// for (int i = 0; i < nearCells.Count; i++)
		int32_t L_19 = V_1;
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_20 = V_0;
		int32_t L_21;
		L_21 = List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_inline(L_20, /*hidden argument*/List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_RuntimeMethod_var);
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_00a9;
		}
	}

IL_00c2:
	{
		// }
		return;
	}
}
// System.Void CellScript::MarkCell()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_MarkCell_m1CEB7D6C1528F8812859B33F66E3AF876970B361 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, const RuntimeMethod* method)
{
	{
		// if (!gc.IsBombsPlaced)
		GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * L_0 = __this->get_gc_4();
		bool L_1 = L_0->get_IsBombsPlaced_14();
		if (L_1)
		{
			goto IL_0014;
		}
	}
	{
		// OpenCell();
		CellScript_OpenCell_m6D08D2BA5A1BBA51FCC9CF866349AD92FE3850BC(__this, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0014:
	{
		// else if (IsMarked)
		bool L_2 = __this->get_IsMarked_15();
		if (!L_2)
		{
			goto IL_0030;
		}
	}
	{
		// IsMarked = false;
		__this->set_IsMarked_15((bool)0);
		// FrontImage.sprite = null;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_3 = __this->get_FrontImage_5();
		Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4(L_3, (Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 *)NULL, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0030:
	{
		// IsMarked = true;
		__this->set_IsMarked_15((bool)1);
		// FrontImage.sprite = FlagImage;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_4 = __this->get_FrontImage_5();
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_5 = __this->get_FlagImage_7();
		Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4(L_4, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CellScript::OpenNearCells()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_OpenNearCells_m8D67AD843269C6D9711A83FE6B2900FFF42A8617 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAE6EFDC0C09F841BA88F172CEE94A07B1705B0AF);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		// List<CellScript> nearCells = gc.GetNeighbourCells(i, j);
		GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * L_0 = __this->get_gc_4();
		int32_t L_1 = __this->get_i_11();
		int32_t L_2 = __this->get_j_12();
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_3;
		L_3 = GameController_GetNeighbourCells_m28277D2E82B4A26521D4FD6B03727C451D3EC1DB(L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// if (!gc.IsBombsPlaced)
		GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * L_4 = __this->get_gc_4();
		bool L_5 = L_4->get_IsBombsPlaced_14();
		if (L_5)
		{
			goto IL_002c;
		}
	}
	{
		// OpenCell();
		CellScript_OpenCell_m6D08D2BA5A1BBA51FCC9CF866349AD92FE3850BC(__this, /*hidden argument*/NULL);
		// }
		return;
	}

IL_002c:
	{
		// else if (!IsFlippedToBack)
		bool L_6 = __this->get_IsFlippedToBack_16();
		if (!L_6)
		{
			goto IL_00da;
		}
	}
	{
		// else if (IsEqualsBombsAndMarkersCount(nearCells))
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_7 = V_0;
		bool L_8;
		L_8 = CellScript_IsEqualsBombsAndMarkersCount_mCE82439D14A55EA840FEF814C5DC2A4763C83DEC(__this, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00a2;
		}
	}
	{
		// if (IsRightMarking(nearCells))
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_9 = V_0;
		bool L_10;
		L_10 = CellScript_IsRightMarking_m54D63C04E0117E412BF3509668D6BB27F66F8DDB(__this, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		// for (int i = 0; i < nearCells.Count; i++)
		V_1 = 0;
		goto IL_005d;
	}

IL_004d:
	{
		// nearCells[i].OpenCell();
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_11 = V_0;
		int32_t L_12 = V_1;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_13;
		L_13 = List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_inline(L_11, L_12, /*hidden argument*/List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_RuntimeMethod_var);
		CellScript_OpenCell_m6D08D2BA5A1BBA51FCC9CF866349AD92FE3850BC(L_13, /*hidden argument*/NULL);
		// for (int i = 0; i < nearCells.Count; i++)
		int32_t L_14 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_005d:
	{
		// for (int i = 0; i < nearCells.Count; i++)
		int32_t L_15 = V_1;
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_16 = V_0;
		int32_t L_17;
		L_17 = List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_inline(L_16, /*hidden argument*/List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_RuntimeMethod_var);
		if ((((int32_t)L_15) < ((int32_t)L_17)))
		{
			goto IL_004d;
		}
	}
	{
		// }
		return;
	}

IL_0067:
	{
		// for (int i = 0; i < nearCells.Count; i++)
		V_2 = 0;
		goto IL_0098;
	}

IL_006b:
	{
		// if (nearCells[i].IsBomb && !nearCells[i].IsMarked)
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_18 = V_0;
		int32_t L_19 = V_2;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_20;
		L_20 = List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_inline(L_18, L_19, /*hidden argument*/List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_RuntimeMethod_var);
		bool L_21 = L_20->get_IsBomb_14();
		if (!L_21)
		{
			goto IL_0094;
		}
	}
	{
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_22 = V_0;
		int32_t L_23 = V_2;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_24;
		L_24 = List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_inline(L_22, L_23, /*hidden argument*/List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_RuntimeMethod_var);
		bool L_25 = L_24->get_IsMarked_15();
		if (L_25)
		{
			goto IL_0094;
		}
	}
	{
		// nearCells[i].OpenCell();
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_26 = V_0;
		int32_t L_27 = V_2;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_28;
		L_28 = List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_inline(L_26, L_27, /*hidden argument*/List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_RuntimeMethod_var);
		CellScript_OpenCell_m6D08D2BA5A1BBA51FCC9CF866349AD92FE3850BC(L_28, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_0094:
	{
		// for (int i = 0; i < nearCells.Count; i++)
		int32_t L_29 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)1));
	}

IL_0098:
	{
		// for (int i = 0; i < nearCells.Count; i++)
		int32_t L_30 = V_2;
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_31 = V_0;
		int32_t L_32;
		L_32 = List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_inline(L_31, /*hidden argument*/List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_RuntimeMethod_var);
		if ((((int32_t)L_30) < ((int32_t)L_32)))
		{
			goto IL_006b;
		}
	}
	{
		// }
		return;
	}

IL_00a2:
	{
		// print("markers != bombs");
		MonoBehaviour_print_m4F113B89EC1C221CAC6EC64365E6DAD0AF86F090(_stringLiteralAE6EFDC0C09F841BA88F172CEE94A07B1705B0AF, /*hidden argument*/NULL);
		// for (int i = 0; i < nearCells.Count; i++)
		V_3 = 0;
		goto IL_00d1;
	}

IL_00b0:
	{
		// if (nearCells[i].IsMarked)
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_33 = V_0;
		int32_t L_34 = V_3;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_35;
		L_35 = List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_inline(L_33, L_34, /*hidden argument*/List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_RuntimeMethod_var);
		bool L_36 = L_35->get_IsMarked_15();
		if (!L_36)
		{
			goto IL_00cd;
		}
	}
	{
		// nearCells[i].StartVibrate(20, 4);
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_37 = V_0;
		int32_t L_38 = V_3;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_39;
		L_39 = List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_inline(L_37, L_38, /*hidden argument*/List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_RuntimeMethod_var);
		CellScript_StartVibrate_m68C587BAA15ECC2C23DF1A398EEFE58ACAC07FD5(L_39, ((int32_t)20), 4, /*hidden argument*/NULL);
	}

IL_00cd:
	{
		// for (int i = 0; i < nearCells.Count; i++)
		int32_t L_40 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_40, (int32_t)1));
	}

IL_00d1:
	{
		// for (int i = 0; i < nearCells.Count; i++)
		int32_t L_41 = V_3;
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_42 = V_0;
		int32_t L_43;
		L_43 = List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_inline(L_42, /*hidden argument*/List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_RuntimeMethod_var);
		if ((((int32_t)L_41) < ((int32_t)L_43)))
		{
			goto IL_00b0;
		}
	}

IL_00da:
	{
		// }
		return;
	}
}
// System.Boolean CellScript::IsRightMarking(System.Collections.Generic.List`1<CellScript>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CellScript_IsRightMarking_m54D63C04E0117E412BF3509668D6BB27F66F8DDB (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * ___nearCells0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		// int rightCells = 0;
		V_0 = 0;
		// int nearBombs = 0;
		V_1 = 0;
		// int nearMarkers = 0;
		V_2 = 0;
		// for (int i = 0; i < nearCells.Count; i++)
		V_3 = 0;
		goto IL_0050;
	}

IL_000a:
	{
		// if (nearCells[i].IsBomb == nearCells[i].IsMarked)
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_0 = ___nearCells0;
		int32_t L_1 = V_3;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_2;
		L_2 = List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_inline(L_0, L_1, /*hidden argument*/List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_RuntimeMethod_var);
		bool L_3 = L_2->get_IsBomb_14();
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_4 = ___nearCells0;
		int32_t L_5 = V_3;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_6;
		L_6 = List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_inline(L_4, L_5, /*hidden argument*/List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_RuntimeMethod_var);
		bool L_7 = L_6->get_IsMarked_15();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_7))))
		{
			goto IL_0028;
		}
	}
	{
		// rightCells++;
		int32_t L_8 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0028:
	{
		// if (nearCells[i].IsBomb)
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_9 = ___nearCells0;
		int32_t L_10 = V_3;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_11;
		L_11 = List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_inline(L_9, L_10, /*hidden argument*/List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_RuntimeMethod_var);
		bool L_12 = L_11->get_IsBomb_14();
		if (!L_12)
		{
			goto IL_003a;
		}
	}
	{
		// nearBombs++;
		int32_t L_13 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_003a:
	{
		// if (nearCells[i].IsMarked)
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_14 = ___nearCells0;
		int32_t L_15 = V_3;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_16;
		L_16 = List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_inline(L_14, L_15, /*hidden argument*/List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_RuntimeMethod_var);
		bool L_17 = L_16->get_IsMarked_15();
		if (!L_17)
		{
			goto IL_004c;
		}
	}
	{
		// nearMarkers++;
		int32_t L_18 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_004c:
	{
		// for (int i = 0; i < nearCells.Count; i++)
		int32_t L_19 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
	}

IL_0050:
	{
		// for (int i = 0; i < nearCells.Count; i++)
		int32_t L_20 = V_3;
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_21 = ___nearCells0;
		int32_t L_22;
		L_22 = List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_inline(L_21, /*hidden argument*/List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_RuntimeMethod_var);
		if ((((int32_t)L_20) < ((int32_t)L_22)))
		{
			goto IL_000a;
		}
	}
	{
		// if (nearBombs == 0)
		int32_t L_23 = V_1;
		if (L_23)
		{
			goto IL_005e;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_005e:
	{
		// if (rightCells != nearCells.Count)
		int32_t L_24 = V_0;
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_25 = ___nearCells0;
		int32_t L_26;
		L_26 = List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_inline(L_25, /*hidden argument*/List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_RuntimeMethod_var);
		if ((((int32_t)L_24) == ((int32_t)L_26)))
		{
			goto IL_0069;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_0069:
	{
		// if (nearMarkers != nearBombs)
		int32_t L_27 = V_2;
		int32_t L_28 = V_1;
		if ((((int32_t)L_27) == ((int32_t)L_28)))
		{
			goto IL_006f;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_006f:
	{
		// return true;
		return (bool)1;
	}
}
// System.Boolean CellScript::IsEqualsBombsAndMarkersCount(System.Collections.Generic.List`1<CellScript>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CellScript_IsEqualsBombsAndMarkersCount_mCE82439D14A55EA840FEF814C5DC2A4763C83DEC (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * ___nearCells0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		// int countBombs = 0;
		V_0 = 0;
		// int countMarkers = 0;
		V_1 = 0;
		// for (int i = 0; i < nearCells.Count; i++)
		V_2 = 0;
		goto IL_0030;
	}

IL_0008:
	{
		// if (nearCells[i].IsMarked) countMarkers++;
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_0 = ___nearCells0;
		int32_t L_1 = V_2;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_2;
		L_2 = List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_inline(L_0, L_1, /*hidden argument*/List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_RuntimeMethod_var);
		bool L_3 = L_2->get_IsMarked_15();
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		// if (nearCells[i].IsMarked) countMarkers++;
		int32_t L_4 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_001a:
	{
		// if (nearCells[i].IsBomb) countBombs++;
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_5 = ___nearCells0;
		int32_t L_6 = V_2;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_7;
		L_7 = List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_inline(L_5, L_6, /*hidden argument*/List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_RuntimeMethod_var);
		bool L_8 = L_7->get_IsBomb_14();
		if (!L_8)
		{
			goto IL_002c;
		}
	}
	{
		// if (nearCells[i].IsBomb) countBombs++;
		int32_t L_9 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_002c:
	{
		// for (int i = 0; i < nearCells.Count; i++)
		int32_t L_10 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0030:
	{
		// for (int i = 0; i < nearCells.Count; i++)
		int32_t L_11 = V_2;
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_12 = ___nearCells0;
		int32_t L_13;
		L_13 = List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_inline(L_12, /*hidden argument*/List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_RuntimeMethod_var);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0008;
		}
	}
	{
		// return (countBombs == countMarkers);
		int32_t L_14 = V_0;
		int32_t L_15 = V_1;
		return (bool)((((int32_t)L_14) == ((int32_t)L_15))? 1 : 0);
	}
}
// System.Void CellScript::Flip(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_Flip_m2FDF12D98C578A46DA54906E33BCE6F47AA2A702 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, bool ___toBackSide0, const RuntimeMethod* method)
{
	{
		// if (toBackSide)
		bool L_0 = ___toBackSide0;
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		// IsFlippedToBack = true;
		__this->set_IsFlippedToBack_16((bool)1);
		// if (IsMarked == true)
		bool L_1 = __this->get_IsMarked_15();
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		// MarkCell();
		CellScript_MarkCell_m1CEB7D6C1528F8812859B33F66E3AF876970B361(__this, /*hidden argument*/NULL);
	}

IL_0018:
	{
		// StartFlip(0f, 180f, countFramesToFlip);
		int32_t L_2 = __this->get_countFramesToFlip_8();
		CellScript_StartFlip_m68EDAC191366E438E78ABFB2301A1810FAD11565(__this, (0.0f), (180.0f), L_2, /*hidden argument*/NULL);
		// }
		return;
	}

IL_002f:
	{
		// IsFlippedToBack = false;
		__this->set_IsFlippedToBack_16((bool)0);
		// StartFlip(180f, 0f, countFramesToFlip);
		int32_t L_3 = __this->get_countFramesToFlip_8();
		CellScript_StartFlip_m68EDAC191366E438E78ABFB2301A1810FAD11565(__this, (180.0f), (0.0f), L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CellScript::SetCellScale(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_SetCellScale_mE30F34C461AF6953FEAE12111E0690040C8EF5C0 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, float ___scaleMultiplier0, const RuntimeMethod* method)
{
	{
		// transform.localScale = new Vector3(1f * scaleMultiplier, 1f * scaleMultiplier, 0.1f);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		float L_1 = ___scaleMultiplier0;
		float L_2 = ___scaleMultiplier0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_3), ((float)il2cpp_codegen_multiply((float)(1.0f), (float)L_1)), ((float)il2cpp_codegen_multiply((float)(1.0f), (float)L_2)), (0.100000001f), /*hidden argument*/NULL);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_0, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CellScript::StartFlip(System.Single,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_StartFlip_m68EDAC191366E438E78ABFB2301A1810FAD11565 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, float ___startRotation0, float ___endRotation1, int32_t ___countFrames2, const RuntimeMethod* method)
{
	{
		// if (countFrames == -1)
		int32_t L_0 = ___countFrames2;
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_000c;
		}
	}
	{
		// countFrames = countFramesToFlip;
		int32_t L_1 = __this->get_countFramesToFlip_8();
		___countFrames2 = L_1;
	}

IL_000c:
	{
		// StartCoroutine(FlipAnim(startRotation, endRotation, countFrames));
		float L_2 = ___startRotation0;
		float L_3 = ___endRotation1;
		int32_t L_4 = ___countFrames2;
		RuntimeObject* L_5;
		L_5 = CellScript_FlipAnim_mF440633B7B3F7B578A1F18481670F9A95E0BE2B6(__this, L_2, L_3, L_4, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_6;
		L_6 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CellScript::StartVibrate(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_StartVibrate_m68C587BAA15ECC2C23DF1A398EEFE58ACAC07FD5 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, int32_t ___countFrames0, int32_t ___deltaFrames1, const RuntimeMethod* method)
{
	{
		// if (countFrames == -1)
		int32_t L_0 = ___countFrames0;
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_000c;
		}
	}
	{
		// countFrames = countFramesToVibrate;
		int32_t L_1 = __this->get_countFramesToVibrate_9();
		___countFrames0 = L_1;
	}

IL_000c:
	{
		// if (deltaFrames == -1)
		int32_t L_2 = ___deltaFrames1;
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0018;
		}
	}
	{
		// deltaFrames = countFramesToSemiVibrate;
		int32_t L_3 = __this->get_countFramesToSemiVibrate_10();
		___deltaFrames1 = L_3;
	}

IL_0018:
	{
		// StartCoroutine(VibrateAnim(countFrames, deltaFrames));
		int32_t L_4 = ___countFrames0;
		int32_t L_5 = ___deltaFrames1;
		RuntimeObject* L_6;
		L_6 = CellScript_VibrateAnim_m6A77E2934C12D43DBDDA4AF2B2AAE53FCFE7C415(__this, L_4, L_5, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_7;
		L_7 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_6, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator CellScript::FlipAnim(System.Single,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CellScript_FlipAnim_mF440633B7B3F7B578A1F18481670F9A95E0BE2B6 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, float ___startRotation0, float ___endRotation1, int32_t ___countFrames2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC * L_0 = (U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC *)il2cpp_codegen_object_new(U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC_il2cpp_TypeInfo_var);
		U3CFlipAnimU3Ed__32__ctor_m3C8306BE011D741F286EEF99EF338678674A5B20(L_0, 0, /*hidden argument*/NULL);
		U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC * L_1 = L_0;
		L_1->set_U3CU3E4__this_5(__this);
		U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC * L_2 = L_1;
		float L_3 = ___startRotation0;
		L_2->set_startRotation_3(L_3);
		U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC * L_4 = L_2;
		float L_5 = ___endRotation1;
		L_4->set_endRotation_2(L_5);
		U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC * L_6 = L_4;
		int32_t L_7 = ___countFrames2;
		L_6->set_countFrames_4(L_7);
		return L_6;
	}
}
// System.Collections.IEnumerator CellScript::VibrateAnim(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CellScript_VibrateAnim_m6A77E2934C12D43DBDDA4AF2B2AAE53FCFE7C415 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, int32_t ___countFrames0, int32_t ___deltaFrames1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE * L_0 = (U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE *)il2cpp_codegen_object_new(U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE_il2cpp_TypeInfo_var);
		U3CVibrateAnimU3Ed__33__ctor_m1C4133168D5711366DB5305F2A4FBE525697AE9F(L_0, 0, /*hidden argument*/NULL);
		U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE * L_1 = L_0;
		L_1->set_U3CU3E4__this_2(__this);
		U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE * L_2 = L_1;
		int32_t L_3 = ___countFrames0;
		L_2->set_countFrames_4(L_3);
		U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE * L_4 = L_2;
		int32_t L_5 = ___deltaFrames1;
		L_4->set_deltaFrames_3(L_5);
		return L_4;
	}
}
// System.Void CellScript::SetActivityState(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_SetActivityState_mB96C73DD2424B3A37A0BCDFA8E93AE3B26681C18 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, bool ___newState0, const RuntimeMethod* method)
{
	{
		// if (!newState)
		bool L_0 = ___newState0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		// SetCellScale(1f);
		CellScript_SetCellScale_mE30F34C461AF6953FEAE12111E0690040C8EF5C0(__this, (1.0f), /*hidden argument*/NULL);
	}

IL_000e:
	{
		// IsActive = newState;
		bool L_1 = ___newState0;
		__this->set_IsActive_17(L_1);
		// }
		return;
	}
}
// System.Void CellScript::SetBombState(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_SetBombState_m34FA5F4F7839C8343B34D2AB0A2729B3EDB95F7C (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, bool ___newState0, const RuntimeMethod* method)
{
	{
		// IsBomb = newState;
		bool L_0 = ___newState0;
		__this->set_IsBomb_14(L_0);
		// }
		return;
	}
}
// System.Void CellScript::setFlipState(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_setFlipState_mB4694F313418E59102D46FD926BB5CC584F28ABF (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, bool ___newState0, const RuntimeMethod* method)
{
	{
		// IsFlippedToBack = newState;
		bool L_0 = ___newState0;
		__this->set_IsFlippedToBack_16(L_0);
		// }
		return;
	}
}
// System.Void CellScript::setMarkedState(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_setMarkedState_m464FBE7E2E19E707F61160CAF5D92FCFB82001A0 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, bool ___newState0, const RuntimeMethod* method)
{
	{
		// IsMarked = newState;
		bool L_0 = ___newState0;
		__this->set_IsMarked_15(L_0);
		// }
		return;
	}
}
// System.Void CellScript::Lose_FirstStage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_Lose_FirstStage_m3556D5B3504B23B58F53C76852DAA56010F540A7 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, const RuntimeMethod* method)
{
	{
		// gc.SetAllCellsActivityState(false);
		GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * L_0 = __this->get_gc_4();
		GameController_SetAllCellsActivityState_mC3537CC4BD1207FD2A2BDBC9B2442A0A25AD494C(L_0, (bool)0, /*hidden argument*/NULL);
		// gc.FlipAllCellsWithBombs();
		GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * L_1 = __this->get_gc_4();
		GameController_FlipAllCellsWithBombs_m4202509D2C61906811F15E82256698A07BB4CC61(L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CellScript::Lose_SecondStage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript_Lose_SecondStage_m456CD543979BAF829C837714990F7238A55B3126 (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, const RuntimeMethod* method)
{
	{
		// gc.Restart();
		GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * L_0 = __this->get_gc_4();
		GameController_Restart_m4AEFD85E727195AC4517C6FBE488E42A584527B4(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CellScript::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellScript__ctor_m58CAC33B9D65DE6327E6BEDF4493DCB22844860C (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GameController::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_Start_mCC3F0292799528323E2217A12DB08D98CDD492AE (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, const RuntimeMethod* method)
{
	{
		// StartNewGame();
		GameController_StartNewGame_mD49E9790ABE830D71AD16E830070AD05FFF3E6BC(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameController::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_Update_mA76A2CE1F2AC1AACCDBF913CA6E1EA73DC621CD0 (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void GameController::FillWithEmptyCells()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_FillWithEmptyCells_mB48BDD3791108F6107BF9F8700EF94D034AE90EF (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisCellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA_mB5D3ED8FF21204C96C68B4F221507BBC7E46878C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral174469F0DC1D2C012CBA679BC2295ABD0834AFB7);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * V_8 = NULL;
	int32_t V_9 = 0;
	{
		// for (int i = 0; i < CellsContainer.childCount; i++)
		V_5 = 0;
		goto IL_0022;
	}

IL_0005:
	{
		// Destroy(CellsContainer.GetChild(i).gameObject);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_CellsContainer_5();
		int32_t L_1 = V_5;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_0, L_1, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3;
		L_3 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_3, /*hidden argument*/NULL);
		// for (int i = 0; i < CellsContainer.childCount; i++)
		int32_t L_4 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_0022:
	{
		// for (int i = 0; i < CellsContainer.childCount; i++)
		int32_t L_5 = V_5;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6 = __this->get_CellsContainer_5();
		int32_t L_7;
		L_7 = Transform_get_childCount_mCBED4F6D3F6A7386C4D97C2C3FD25C383A0BCD05(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0005;
		}
	}
	{
		// float spawnPositionX = 0;
		V_0 = (0.0f);
		// float spawnPositionY = 0;
		V_1 = (0.0f);
		// int numberOfCell = 0;
		V_2 = 0;
		// for (int i = 0; i < CountCellsInColumn; i++)
		V_6 = 0;
		goto IL_010d;
	}

IL_0047:
	{
		// spawnPositionX = 0;
		V_0 = (0.0f);
		// for (int j = 0; j < CountCellsInLine; j++)
		V_7 = 0;
		goto IL_00f1;
	}

IL_0055:
	{
		// GameObject cell = Instantiate(CellPrefab, new Vector3(spawnPositionX, spawnPositionY, 0), Quaternion.identity);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = __this->get_CellPrefab_6();
		float L_9 = V_0;
		float L_10 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		memset((&L_11), 0, sizeof(L_11));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_11), L_9, L_10, (0.0f), /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_12;
		L_12 = Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_13;
		L_13 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B(L_8, L_11, L_12, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		// cell.transform.parent = CellsContainer;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_14 = L_13;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_15;
		L_15 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_14, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16 = __this->get_CellsContainer_5();
		Transform_set_parent_mEAE304E1A804E8B83054CEECB5BF1E517196EC13(L_15, L_16, /*hidden argument*/NULL);
		// cell.name = "Cell " + numberOfCell++;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_17 = L_14;
		int32_t L_18 = V_2;
		int32_t L_19 = L_18;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
		V_9 = L_19;
		String_t* L_20;
		L_20 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_9), /*hidden argument*/NULL);
		String_t* L_21;
		L_21 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral174469F0DC1D2C012CBA679BC2295ABD0834AFB7, L_20, /*hidden argument*/NULL);
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(L_17, L_21, /*hidden argument*/NULL);
		// CellScript buffCellScript = cell.GetComponent<CellScript>();
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_22;
		L_22 = GameObject_GetComponent_TisCellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA_mB5D3ED8FF21204C96C68B4F221507BBC7E46878C(L_17, /*hidden argument*/GameObject_GetComponent_TisCellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA_mB5D3ED8FF21204C96C68B4F221507BBC7E46878C_RuntimeMethod_var);
		V_8 = L_22;
		// buffCellScript.gc = this;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_23 = V_8;
		L_23->set_gc_4(__this);
		// buffCellScript.i = i;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_24 = V_8;
		int32_t L_25 = V_6;
		L_24->set_i_11(L_25);
		// buffCellScript.j = j;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_26 = V_8;
		int32_t L_27 = V_7;
		L_26->set_j_12(L_27);
		// buffCellScript.FlagImage = tiles[9];
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_28 = V_8;
		SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* L_29 = __this->get_tiles_13();
		int32_t L_30 = ((int32_t)9);
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_31 = (L_29)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_30));
		L_28->set_FlagImage_7(L_31);
		// cellScripts[i, j] = buffCellScript; // добавляем ссылку на скрипт в 2мерный массив
		CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* L_32 = __this->get_cellScripts_17();
		int32_t L_33 = V_6;
		int32_t L_34 = V_7;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_35 = V_8;
		(L_32)->SetAtUnchecked(L_33, L_34, L_35);
		// spawnPositionX += DistanceBetweenCells;
		float L_36 = V_0;
		float L_37 = __this->get_DistanceBetweenCells_9();
		V_0 = ((float)il2cpp_codegen_add((float)L_36, (float)L_37));
		// for (int j = 0; j < CountCellsInLine; j++)
		int32_t L_38 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_38, (int32_t)1));
	}

IL_00f1:
	{
		// for (int j = 0; j < CountCellsInLine; j++)
		int32_t L_39 = V_7;
		int32_t L_40 = __this->get_CountCellsInLine_7();
		if ((((int32_t)L_39) < ((int32_t)L_40)))
		{
			goto IL_0055;
		}
	}
	{
		// spawnPositionY -= DistanceBetweenCells;
		float L_41 = V_1;
		float L_42 = __this->get_DistanceBetweenCells_9();
		V_1 = ((float)il2cpp_codegen_subtract((float)L_41, (float)L_42));
		// for (int i = 0; i < CountCellsInColumn; i++)
		int32_t L_43 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_43, (int32_t)1));
	}

IL_010d:
	{
		// for (int i = 0; i < CountCellsInColumn; i++)
		int32_t L_44 = V_6;
		int32_t L_45 = __this->get_CountCellsInColumn_8();
		if ((((int32_t)L_44) < ((int32_t)L_45)))
		{
			goto IL_0047;
		}
	}
	{
		// countCells = numberOfCell;
		int32_t L_46 = V_2;
		__this->set_countCells_18(L_46);
		// float camPosX = (CellsContainer.GetChild(CellsContainer.childCount - 1).position.x - CellsContainer.GetChild(0).position.x) / 2f;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_47 = __this->get_CellsContainer_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_48 = __this->get_CellsContainer_5();
		int32_t L_49;
		L_49 = Transform_get_childCount_mCBED4F6D3F6A7386C4D97C2C3FD25C383A0BCD05(L_48, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_50;
		L_50 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_47, ((int32_t)il2cpp_codegen_subtract((int32_t)L_49, (int32_t)1)), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_51;
		L_51 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_50, /*hidden argument*/NULL);
		float L_52 = L_51.get_x_2();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_53 = __this->get_CellsContainer_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_54;
		L_54 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_53, 0, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_55;
		L_55 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_54, /*hidden argument*/NULL);
		float L_56 = L_55.get_x_2();
		V_3 = ((float)((float)((float)il2cpp_codegen_subtract((float)L_52, (float)L_56))/(float)(2.0f)));
		// float camPosY = (CellsContainer.GetChild(CellsContainer.childCount - 1).position.y - CellsContainer.GetChild(0).position.y) / 2f;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_57 = __this->get_CellsContainer_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_58 = __this->get_CellsContainer_5();
		int32_t L_59;
		L_59 = Transform_get_childCount_mCBED4F6D3F6A7386C4D97C2C3FD25C383A0BCD05(L_58, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_60;
		L_60 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_57, ((int32_t)il2cpp_codegen_subtract((int32_t)L_59, (int32_t)1)), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_61;
		L_61 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_60, /*hidden argument*/NULL);
		float L_62 = L_61.get_y_3();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_63 = __this->get_CellsContainer_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_64;
		L_64 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_63, 0, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_65;
		L_65 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_64, /*hidden argument*/NULL);
		float L_66 = L_65.get_y_3();
		V_4 = ((float)((float)((float)il2cpp_codegen_subtract((float)L_62, (float)L_66))/(float)(2.0f)));
		// Camera.transform.position = new Vector3(camPosX, camPosY, -10f);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_67 = __this->get_Camera_4();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_68;
		L_68 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_67, /*hidden argument*/NULL);
		float L_69 = V_3;
		float L_70 = V_4;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_71;
		memset((&L_71), 0, sizeof(L_71));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_71), L_69, L_70, (-10.0f), /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_68, L_71, /*hidden argument*/NULL);
		// if (CountCellsInColumn > CountCellsInLine)
		int32_t L_72 = __this->get_CountCellsInColumn_8();
		int32_t L_73 = __this->get_CountCellsInLine_7();
		if ((((int32_t)L_72) <= ((int32_t)L_73)))
		{
			goto IL_01ec;
		}
	}
	{
		// Camera.orthographicSize = CountCellsInColumn / 2f + 1f;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_74 = __this->get_Camera_4();
		int32_t L_75 = __this->get_CountCellsInColumn_8();
		Camera_set_orthographicSize_mFC4BBB0BB0097A5FE13E99D8388DF3008971085F(L_74, ((float)il2cpp_codegen_add((float)((float)((float)((float)((float)L_75))/(float)(2.0f))), (float)(1.0f))), /*hidden argument*/NULL);
		return;
	}

IL_01ec:
	{
		// Camera.orthographicSize = CountCellsInLine / 2f + 1f;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_76 = __this->get_Camera_4();
		int32_t L_77 = __this->get_CountCellsInLine_7();
		Camera_set_orthographicSize_mFC4BBB0BB0097A5FE13E99D8388DF3008971085F(L_76, ((float)il2cpp_codegen_add((float)((float)((float)((float)((float)L_77))/(float)(2.0f))), (float)(1.0f))), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameController::FillInEmptyCells(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_FillInEmptyCells_mAAC291E6668B3D54CC37DCB1EE9B368D793096D3 (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, int32_t ___col0, int32_t ___lin1, const RuntimeMethod* method)
{
	{
		// PlaceBombsAtCells(col, lin);
		int32_t L_0 = ___col0;
		int32_t L_1 = ___lin1;
		GameController_PlaceBombsAtCells_m59E386DFFD30EC4E86BD51BD725452EF4DD9D407(__this, L_0, L_1, /*hidden argument*/NULL);
		// CalculateNumbersOnCells();
		GameController_CalculateNumbersOnCells_mE277990E457A0D9444ADCEAB22F297D5B7FA6718(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameController::PlaceBombsAtCells(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_PlaceBombsAtCells_m59E386DFFD30EC4E86BD51BD725452EF4DD9D407 (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, int32_t ___voidCol0, int32_t ___voidLin1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral46B78FC21D3932AA58CC88ED781E7FE2D3AB98E8);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		// int placedBombs = 0;
		V_0 = 0;
		// if (CountBombs > countCells - 9)
		int32_t L_0 = __this->get_CountBombs_10();
		int32_t L_1 = __this->get_countCells_18();
		if ((((int32_t)L_0) <= ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)((int32_t)9))))))
		{
			goto IL_00a6;
		}
	}
	{
		// Debug.LogError("Too much bombs to place...");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(_stringLiteral46B78FC21D3932AA58CC88ED781E7FE2D3AB98E8, /*hidden argument*/NULL);
		// placedBombs = CountBombs;
		int32_t L_2 = __this->get_CountBombs_10();
		V_0 = L_2;
		goto IL_00a6;
	}

IL_0029:
	{
		// int col = Random.Range(0, CountCellsInColumn);
		int32_t L_3 = __this->get_CountCellsInColumn_8();
		int32_t L_4;
		L_4 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		// int lin = Random.Range(0, CountCellsInLine);
		int32_t L_5 = __this->get_CountCellsInLine_7();
		int32_t L_6;
		L_6 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		// if (cellScripts[col, lin].IsBomb == false)
		CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* L_7 = __this->get_cellScripts_17();
		int32_t L_8 = V_1;
		int32_t L_9 = V_2;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_10;
		L_10 = (L_7)->GetAtUnchecked(L_8, L_9);
		bool L_11 = L_10->get_IsBomb_14();
		if (L_11)
		{
			goto IL_00a6;
		}
	}
	{
		// if (col < voidCol - 1 || col > voidCol + 1 || lin < voidLin - 1 || lin > voidLin + 1)
		int32_t L_12 = V_1;
		int32_t L_13 = ___voidCol0;
		if ((((int32_t)L_12) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_13, (int32_t)1)))))
		{
			goto IL_006f;
		}
	}
	{
		int32_t L_14 = V_1;
		int32_t L_15 = ___voidCol0;
		if ((((int32_t)L_14) > ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1)))))
		{
			goto IL_006f;
		}
	}
	{
		int32_t L_16 = V_2;
		int32_t L_17 = ___voidLin1;
		if ((((int32_t)L_16) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_17, (int32_t)1)))))
		{
			goto IL_006f;
		}
	}
	{
		int32_t L_18 = V_2;
		int32_t L_19 = ___voidLin1;
		if ((((int32_t)L_18) <= ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1)))))
		{
			goto IL_00a6;
		}
	}

IL_006f:
	{
		// cellScripts[col, lin].SetBombState(true);
		CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* L_20 = __this->get_cellScripts_17();
		int32_t L_21 = V_1;
		int32_t L_22 = V_2;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_23;
		L_23 = (L_20)->GetAtUnchecked(L_21, L_22);
		CellScript_SetBombState_m34FA5F4F7839C8343B34D2AB0A2729B3EDB95F7C_inline(L_23, (bool)1, /*hidden argument*/NULL);
		// cellScripts[col, lin].BackImage.sprite = tiles[10];
		CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* L_24 = __this->get_cellScripts_17();
		int32_t L_25 = V_1;
		int32_t L_26 = V_2;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_27;
		L_27 = (L_24)->GetAtUnchecked(L_25, L_26);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_28 = L_27->get_BackImage_6();
		SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* L_29 = __this->get_tiles_13();
		int32_t L_30 = ((int32_t)10);
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_31 = (L_29)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_30));
		Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4(L_28, L_31, /*hidden argument*/NULL);
		// placedBombs++;
		int32_t L_32 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_32, (int32_t)1));
	}

IL_00a6:
	{
		// while (placedBombs < CountBombs)
		int32_t L_33 = V_0;
		int32_t L_34 = __this->get_CountBombs_10();
		if ((((int32_t)L_33) < ((int32_t)L_34)))
		{
			goto IL_0029;
		}
	}
	{
		// IsBombsPlaced = true;
		__this->set_IsBombsPlaced_14((bool)1);
		// }
		return;
	}
}
// System.Void GameController::CalculateNumbersOnCells()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_CalculateNumbersOnCells_mE277990E457A0D9444ADCEAB22F297D5B7FA6718 (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * V_3 = NULL;
	int32_t V_4 = 0;
	{
		// for (int i = 0; i < CountCellsInColumn; i++)
		V_0 = 0;
		goto IL_0098;
	}

IL_0007:
	{
		// for (int j = 0; j < CountCellsInLine; j++)
		V_1 = 0;
		goto IL_0088;
	}

IL_000b:
	{
		// if (cellScripts[i, j].IsBomb)
		CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* L_0 = __this->get_cellScripts_17();
		int32_t L_1 = V_0;
		int32_t L_2 = V_1;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_3;
		L_3 = (L_0)->GetAtUnchecked(L_1, L_2);
		bool L_4 = L_3->get_IsBomb_14();
		if (L_4)
		{
			goto IL_0084;
		}
	}
	{
		// int countNearBombs = 0;
		V_2 = 0;
		// List<CellScript> nearCells = GetNeighbourCells(i, j);
		int32_t L_5 = V_0;
		int32_t L_6 = V_1;
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_7;
		L_7 = GameController_GetNeighbourCells_m28277D2E82B4A26521D4FD6B03727C451D3EC1DB(__this, L_5, L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		// for (int k = 0; k < nearCells.Count; k++)
		V_4 = 0;
		goto IL_0048;
	}

IL_002f:
	{
		// if (nearCells[k].IsBomb)
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_8 = V_3;
		int32_t L_9 = V_4;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_10;
		L_10 = List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_inline(L_8, L_9, /*hidden argument*/List_1_get_Item_m820D8FCAA045E26AD77DF252587B3DFF8AAC38A9_RuntimeMethod_var);
		bool L_11 = L_10->get_IsBomb_14();
		if (!L_11)
		{
			goto IL_0042;
		}
	}
	{
		// countNearBombs++;
		int32_t L_12 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
	}

IL_0042:
	{
		// for (int k = 0; k < nearCells.Count; k++)
		int32_t L_13 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0048:
	{
		// for (int k = 0; k < nearCells.Count; k++)
		int32_t L_14 = V_4;
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_15 = V_3;
		int32_t L_16;
		L_16 = List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_inline(L_15, /*hidden argument*/List_1_get_Count_mE8238DD999A57BBD9AA3EAB689B2C8AB185C748C_RuntimeMethod_var);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_002f;
		}
	}
	{
		// cellScripts[i, j].CountNearBombs = countNearBombs;
		CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* L_17 = __this->get_cellScripts_17();
		int32_t L_18 = V_0;
		int32_t L_19 = V_1;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_20;
		L_20 = (L_17)->GetAtUnchecked(L_18, L_19);
		int32_t L_21 = V_2;
		L_20->set_CountNearBombs_13(L_21);
		// cellScripts[i,j].BackImage.sprite = tiles[countNearBombs];
		CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* L_22 = __this->get_cellScripts_17();
		int32_t L_23 = V_0;
		int32_t L_24 = V_1;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_25;
		L_25 = (L_22)->GetAtUnchecked(L_23, L_24);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_26 = L_25->get_BackImage_6();
		SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* L_27 = __this->get_tiles_13();
		int32_t L_28 = V_2;
		int32_t L_29 = L_28;
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_30 = (L_27)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_29));
		Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4(L_26, L_30, /*hidden argument*/NULL);
	}

IL_0084:
	{
		// for (int j = 0; j < CountCellsInLine; j++)
		int32_t L_31 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_31, (int32_t)1));
	}

IL_0088:
	{
		// for (int j = 0; j < CountCellsInLine; j++)
		int32_t L_32 = V_1;
		int32_t L_33 = __this->get_CountCellsInLine_7();
		if ((((int32_t)L_32) < ((int32_t)L_33)))
		{
			goto IL_000b;
		}
	}
	{
		// for (int i = 0; i < CountCellsInColumn; i++)
		int32_t L_34 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_34, (int32_t)1));
	}

IL_0098:
	{
		// for (int i = 0; i < CountCellsInColumn; i++)
		int32_t L_35 = V_0;
		int32_t L_36 = __this->get_CountCellsInColumn_8();
		if ((((int32_t)L_35) < ((int32_t)L_36)))
		{
			goto IL_0007;
		}
	}
	{
		// }
		return;
	}
}
// System.Void GameController::ClearField()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_ClearField_mE9971555CA72D52E8ACD2FDB7E60DB7263A9BDFA (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// for (int i = 0; i < CellsContainer.childCount; i++)
		V_0 = 0;
		goto IL_001e;
	}

IL_0004:
	{
		// Destroy(CellsContainer.GetChild(i).gameObject);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_CellsContainer_5();
		int32_t L_1 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_0, L_1, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3;
		L_3 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_3, /*hidden argument*/NULL);
		// for (int i = 0; i < CellsContainer.childCount; i++)
		int32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_001e:
	{
		// for (int i = 0; i < CellsContainer.childCount; i++)
		int32_t L_5 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6 = __this->get_CellsContainer_5();
		int32_t L_7;
		L_7 = Transform_get_childCount_mCBED4F6D3F6A7386C4D97C2C3FD25C383A0BCD05(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// System.Void GameController::FlipAllCellsWithBombs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_FlipAllCellsWithBombs_m4202509D2C61906811F15E82256698A07BB4CC61 (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// for (int i = 0; i < CountCellsInColumn; i++)
		V_0 = 0;
		goto IL_0054;
	}

IL_0004:
	{
		// for (int j = 0; j < CountCellsInLine; j++)
		V_1 = 0;
		goto IL_0047;
	}

IL_0008:
	{
		// if (cellScripts[i, j].IsBomb == true)
		CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* L_0 = __this->get_cellScripts_17();
		int32_t L_1 = V_0;
		int32_t L_2 = V_1;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_3;
		L_3 = (L_0)->GetAtUnchecked(L_1, L_2);
		bool L_4 = L_3->get_IsBomb_14();
		if (!L_4)
		{
			goto IL_0043;
		}
	}
	{
		// cellScripts[i, j].Flip(true);
		CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* L_5 = __this->get_cellScripts_17();
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_8;
		L_8 = (L_5)->GetAtUnchecked(L_6, L_7);
		CellScript_Flip_m2FDF12D98C578A46DA54906E33BCE6F47AA2A702(L_8, (bool)1, /*hidden argument*/NULL);
		// cellScripts[i, j].StartVibrate();
		CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* L_9 = __this->get_cellScripts_17();
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_12;
		L_12 = (L_9)->GetAtUnchecked(L_10, L_11);
		CellScript_StartVibrate_m68C587BAA15ECC2C23DF1A398EEFE58ACAC07FD5(L_12, (-1), (-1), /*hidden argument*/NULL);
	}

IL_0043:
	{
		// for (int j = 0; j < CountCellsInLine; j++)
		int32_t L_13 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0047:
	{
		// for (int j = 0; j < CountCellsInLine; j++)
		int32_t L_14 = V_1;
		int32_t L_15 = __this->get_CountCellsInLine_7();
		if ((((int32_t)L_14) < ((int32_t)L_15)))
		{
			goto IL_0008;
		}
	}
	{
		// for (int i = 0; i < CountCellsInColumn; i++)
		int32_t L_16 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1));
	}

IL_0054:
	{
		// for (int i = 0; i < CountCellsInColumn; i++)
		int32_t L_17 = V_0;
		int32_t L_18 = __this->get_CountCellsInColumn_8();
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// System.Void GameController::SetAllCellsActivityState(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_SetAllCellsActivityState_mC3537CC4BD1207FD2A2BDBC9B2442A0A25AD494C (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, bool ___newState0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// for (int i = 0; i < CountCellsInColumn; i++)
		V_0 = 0;
		goto IL_002c;
	}

IL_0004:
	{
		// for (int j = 0; j < CountCellsInLine; j++)
		V_1 = 0;
		goto IL_001f;
	}

IL_0008:
	{
		// cellScripts[i, j].SetActivityState(newState);
		CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* L_0 = __this->get_cellScripts_17();
		int32_t L_1 = V_0;
		int32_t L_2 = V_1;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_3;
		L_3 = (L_0)->GetAtUnchecked(L_1, L_2);
		bool L_4 = ___newState0;
		CellScript_SetActivityState_mB96C73DD2424B3A37A0BCDFA8E93AE3B26681C18(L_3, L_4, /*hidden argument*/NULL);
		// for (int j = 0; j < CountCellsInLine; j++)
		int32_t L_5 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
	}

IL_001f:
	{
		// for (int j = 0; j < CountCellsInLine; j++)
		int32_t L_6 = V_1;
		int32_t L_7 = __this->get_CountCellsInLine_7();
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0008;
		}
	}
	{
		// for (int i = 0; i < CountCellsInColumn; i++)
		int32_t L_8 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_002c:
	{
		// for (int i = 0; i < CountCellsInColumn; i++)
		int32_t L_9 = V_0;
		int32_t L_10 = __this->get_CountCellsInColumn_8();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// System.Collections.Generic.List`1<CellScript> GameController::GetNeighbourCells(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * GameController_GetNeighbourCells_m28277D2E82B4A26521D4FD6B03727C451D3EC1DB (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, int32_t ___i0, int32_t ___j1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mDBFD9180E77276180C3BB51B5F7A9AA08867934A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mC414BFFA6AB5DA6199FCADA47C75B1AC1641D027_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		// List<CellScript> nearCells = new List<CellScript>();
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_0 = (List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 *)il2cpp_codegen_object_new(List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4_il2cpp_TypeInfo_var);
		List_1__ctor_mC414BFFA6AB5DA6199FCADA47C75B1AC1641D027(L_0, /*hidden argument*/List_1__ctor_mC414BFFA6AB5DA6199FCADA47C75B1AC1641D027_RuntimeMethod_var);
		V_0 = L_0;
		// for (int ii = i - 1; ii <= i + 1; ii++)
		int32_t L_1 = ___i0;
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1));
		goto IL_005c;
	}

IL_000c:
	{
		// for (int jj = j - 1; jj <= j + 1; jj++)
		int32_t L_2 = ___j1;
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1));
		goto IL_0052;
	}

IL_0012:
	{
		// if (ii < 0 || jj < 0 || ii > CountCellsInColumn - 1 || jj > CountCellsInLine - 1 || (ii == i & jj == j))
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) < ((int32_t)0)))
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_4 = V_2;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_5 = V_1;
		int32_t L_6 = __this->get_CountCellsInColumn_8();
		if ((((int32_t)L_5) > ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)1)))))
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_7 = V_2;
		int32_t L_8 = __this->get_CountCellsInLine_7();
		if ((((int32_t)L_7) > ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)1)))))
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___i0;
		int32_t L_11 = V_2;
		int32_t L_12 = ___j1;
		if (((int32_t)((int32_t)((((int32_t)L_9) == ((int32_t)L_10))? 1 : 0)&(int32_t)((((int32_t)L_11) == ((int32_t)L_12))? 1 : 0))))
		{
			goto IL_004e;
		}
	}
	{
		// nearCells.Add(cellScripts[ii, jj]);
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_13 = V_0;
		CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* L_14 = __this->get_cellScripts_17();
		int32_t L_15 = V_1;
		int32_t L_16 = V_2;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_17;
		L_17 = (L_14)->GetAtUnchecked(L_15, L_16);
		List_1_Add_mDBFD9180E77276180C3BB51B5F7A9AA08867934A(L_13, L_17, /*hidden argument*/List_1_Add_mDBFD9180E77276180C3BB51B5F7A9AA08867934A_RuntimeMethod_var);
	}

IL_004e:
	{
		// for (int jj = j - 1; jj <= j + 1; jj++)
		int32_t L_18 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_0052:
	{
		// for (int jj = j - 1; jj <= j + 1; jj++)
		int32_t L_19 = V_2;
		int32_t L_20 = ___j1;
		if ((((int32_t)L_19) <= ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1)))))
		{
			goto IL_0012;
		}
	}
	{
		// for (int ii = i - 1; ii <= i + 1; ii++)
		int32_t L_21 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1));
	}

IL_005c:
	{
		// for (int ii = i - 1; ii <= i + 1; ii++)
		int32_t L_22 = V_1;
		int32_t L_23 = ___i0;
		if ((((int32_t)L_22) <= ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1)))))
		{
			goto IL_000c;
		}
	}
	{
		// return nearCells;
		List_1_t7F5247FA144F993D2097ADD497AFA3DA6AC16AB4 * L_24 = V_0;
		return L_24;
	}
}
// System.Void GameController::FlipUnflippedCells(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_FlipUnflippedCells_mAED35070EB6A6DC4C5B049A4413FD3DFDC8B26A0 (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, bool ___isFlipToBackSide0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// for (int i = 0; i < CountCellsInColumn; i++)
		V_0 = 0;
		goto IL_0041;
	}

IL_0004:
	{
		// for (int j = 0; j < CountCellsInLine; j++)
		V_1 = 0;
		goto IL_0034;
	}

IL_0008:
	{
		// if (cellScripts[i, j].IsFlippedToBack != isFlipToBackSide)
		CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* L_0 = __this->get_cellScripts_17();
		int32_t L_1 = V_0;
		int32_t L_2 = V_1;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_3;
		L_3 = (L_0)->GetAtUnchecked(L_1, L_2);
		bool L_4 = L_3->get_IsFlippedToBack_16();
		bool L_5 = ___isFlipToBackSide0;
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0030;
		}
	}
	{
		// cellScripts[i, j].Flip(isFlipToBackSide);
		CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* L_6 = __this->get_cellScripts_17();
		int32_t L_7 = V_0;
		int32_t L_8 = V_1;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_9;
		L_9 = (L_6)->GetAtUnchecked(L_7, L_8);
		bool L_10 = ___isFlipToBackSide0;
		CellScript_Flip_m2FDF12D98C578A46DA54906E33BCE6F47AA2A702(L_9, L_10, /*hidden argument*/NULL);
	}

IL_0030:
	{
		// for (int j = 0; j < CountCellsInLine; j++)
		int32_t L_11 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_0034:
	{
		// for (int j = 0; j < CountCellsInLine; j++)
		int32_t L_12 = V_1;
		int32_t L_13 = __this->get_CountCellsInLine_7();
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0008;
		}
	}
	{
		// for (int i = 0; i < CountCellsInColumn; i++)
		int32_t L_14 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_0041:
	{
		// for (int i = 0; i < CountCellsInColumn; i++)
		int32_t L_15 = V_0;
		int32_t L_16 = __this->get_CountCellsInColumn_8();
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// System.Void GameController::UnmarkMarkedCells()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_UnmarkMarkedCells_m3D355AC420234321AA8BAAEB30D34F876F48B939 (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// for (int i = 0; i < CountCellsInColumn; i++)
		V_0 = 0;
		goto IL_003f;
	}

IL_0004:
	{
		// for (int j = 0; j < CountCellsInLine; j++)
		V_1 = 0;
		goto IL_0032;
	}

IL_0008:
	{
		// if (cellScripts[i, j].IsMarked == true)
		CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* L_0 = __this->get_cellScripts_17();
		int32_t L_1 = V_0;
		int32_t L_2 = V_1;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_3;
		L_3 = (L_0)->GetAtUnchecked(L_1, L_2);
		bool L_4 = L_3->get_IsMarked_15();
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		// cellScripts[i, j].MarkCell();
		CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* L_5 = __this->get_cellScripts_17();
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_8;
		L_8 = (L_5)->GetAtUnchecked(L_6, L_7);
		CellScript_MarkCell_m1CEB7D6C1528F8812859B33F66E3AF876970B361(L_8, /*hidden argument*/NULL);
	}

IL_002e:
	{
		// for (int j = 0; j < CountCellsInLine; j++)
		int32_t L_9 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0032:
	{
		// for (int j = 0; j < CountCellsInLine; j++)
		int32_t L_10 = V_1;
		int32_t L_11 = __this->get_CountCellsInLine_7();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0008;
		}
	}
	{
		// for (int i = 0; i < CountCellsInColumn; i++)
		int32_t L_12 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
	}

IL_003f:
	{
		// for (int i = 0; i < CountCellsInColumn; i++)
		int32_t L_13 = V_0;
		int32_t L_14 = __this->get_CountCellsInColumn_8();
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// System.Void GameController::StartNewGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_StartNewGame_mD49E9790ABE830D71AD16E830070AD05FFF3E6BC (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// cellScripts = new CellScript[CountCellsInColumn, CountCellsInLine];
		int32_t L_0 = __this->get_CountCellsInColumn_8();
		int32_t L_1 = __this->get_CountCellsInLine_7();
		il2cpp_array_size_t L_3[] = { (il2cpp_array_size_t)L_0, (il2cpp_array_size_t)L_1 };
		CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* L_2 = (CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5*)GenArrayNew(CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5_il2cpp_TypeInfo_var, L_3);
		__this->set_cellScripts_17(L_2);
		// ClearField();
		GameController_ClearField_mE9971555CA72D52E8ACD2FDB7E60DB7263A9BDFA(__this, /*hidden argument*/NULL);
		// FillWithEmptyCells();
		GameController_FillWithEmptyCells_mB48BDD3791108F6107BF9F8700EF94D034AE90EF(__this, /*hidden argument*/NULL);
		// IsBombsPlaced = false;
		__this->set_IsBombsPlaced_14((bool)0);
		// countUnbombedCells = countCells - CountBombs;
		int32_t L_4 = __this->get_countCells_18();
		int32_t L_5 = __this->get_CountBombs_10();
		__this->set_countUnbombedCells_15(((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)L_5)));
		// countMarkedCells = 0;
		__this->set_countMarkedCells_16(0);
		// }
		return;
	}
}
// System.Void GameController::Restart()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_Restart_m4AEFD85E727195AC4517C6FBE488E42A584527B4 (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8C65C0AE1EF18E9EA06D11A2B8151151F9490A0C);
		s_Il2CppMethodInitialized = true;
	}
	{
		// UnmarkMarkedCells();
		GameController_UnmarkMarkedCells_m3D355AC420234321AA8BAAEB30D34F876F48B939(__this, /*hidden argument*/NULL);
		// FlipUnflippedCells(false);
		GameController_FlipUnflippedCells_mAED35070EB6A6DC4C5B049A4413FD3DFDC8B26A0(__this, (bool)0, /*hidden argument*/NULL);
		// Invoke("StartNewGame", AutoRestartTime);
		float L_0 = __this->get_AutoRestartTime_12();
		MonoBehaviour_Invoke_m4AAB759653B1C6FB0653527F4DDC72D1E9162CC4(__this, _stringLiteral8C65C0AE1EF18E9EA06D11A2B8151151F9490A0C, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameController::AddCountMarkedCells(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_AddCountMarkedCells_mECEB80820F153E578871851C3F10512E337E7CA5 (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, int32_t ___addictiveNum0, const RuntimeMethod* method)
{
	{
		// countMarkedCells += addictiveNum;
		int32_t L_0 = __this->get_countMarkedCells_16();
		int32_t L_1 = ___addictiveNum0;
		__this->set_countMarkedCells_16(((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)L_1)));
		// if (countMarkedCells == countUnbombedCells)
		int32_t L_2 = __this->get_countMarkedCells_16();
		int32_t L_3 = __this->get_countUnbombedCells_15();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0022;
		}
	}
	{
		// ProcessWinEvent();
		GameController_ProcessWinEvent_mF1CCF90F21440B39896B28A1A9264617469C2CD0(__this, /*hidden argument*/NULL);
	}

IL_0022:
	{
		// }
		return;
	}
}
// System.Void GameController::ProcessWinEvent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_ProcessWinEvent_mF1CCF90F21440B39896B28A1A9264617469C2CD0 (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral78E0EB8CE240AD8239D26F174DBEFF00C4529892);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral838E394589B5693706685699154545855BEAE0B2);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// print("WIN");
		MonoBehaviour_print_m4F113B89EC1C221CAC6EC64365E6DAD0AF86F090(_stringLiteral78E0EB8CE240AD8239D26F174DBEFF00C4529892, /*hidden argument*/NULL);
		// for (int i = 0; i < CountCellsInColumn; i++)
		V_0 = 0;
		goto IL_004b;
	}

IL_000e:
	{
		// for (int j = 0; j < CountCellsInLine; j++)
		V_1 = 0;
		goto IL_003e;
	}

IL_0012:
	{
		// if (cellScripts[i, j].IsBomb)
		CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* L_0 = __this->get_cellScripts_17();
		int32_t L_1 = V_0;
		int32_t L_2 = V_1;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_3;
		L_3 = (L_0)->GetAtUnchecked(L_1, L_2);
		bool L_4 = L_3->get_IsBomb_14();
		if (!L_4)
		{
			goto IL_003a;
		}
	}
	{
		// cellScripts[i, j].StartVibrate();
		CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* L_5 = __this->get_cellScripts_17();
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_8;
		L_8 = (L_5)->GetAtUnchecked(L_6, L_7);
		CellScript_StartVibrate_m68C587BAA15ECC2C23DF1A398EEFE58ACAC07FD5(L_8, (-1), (-1), /*hidden argument*/NULL);
	}

IL_003a:
	{
		// for (int j = 0; j < CountCellsInLine; j++)
		int32_t L_9 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_003e:
	{
		// for (int j = 0; j < CountCellsInLine; j++)
		int32_t L_10 = V_1;
		int32_t L_11 = __this->get_CountCellsInLine_7();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0012;
		}
	}
	{
		// for (int i = 0; i < CountCellsInColumn; i++)
		int32_t L_12 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
	}

IL_004b:
	{
		// for (int i = 0; i < CountCellsInColumn; i++)
		int32_t L_13 = V_0;
		int32_t L_14 = __this->get_CountCellsInColumn_8();
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_000e;
		}
	}
	{
		// Invoke("Restart", 3f);
		MonoBehaviour_Invoke_m4AAB759653B1C6FB0653527F4DDC72D1E9162CC4(__this, _stringLiteral838E394589B5693706685699154545855BEAE0B2, (3.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameController::CreateTableInConsole()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController_CreateTableInConsole_mE4D6A77243C1D826DB8813E715827A14168F72FB (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8CC2154BA99A342FCE979C29AE9C29FFDC415950);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		// string abc = "";
		V_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		// for (int i = 0; i < CountCellsInColumn; i++)
		V_1 = 0;
		goto IL_0070;
	}

IL_000a:
	{
		// for (int j = 0; j < CountCellsInLine; j++)
		V_2 = 0;
		goto IL_0057;
	}

IL_000e:
	{
		// if (!cellScripts[i, j].IsBomb)
		CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* L_0 = __this->get_cellScripts_17();
		int32_t L_1 = V_1;
		int32_t L_2 = V_2;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_3;
		L_3 = (L_0)->GetAtUnchecked(L_1, L_2);
		bool L_4 = L_3->get_IsBomb_14();
		if (L_4)
		{
			goto IL_0047;
		}
	}
	{
		// abc += cellScripts[i, j].CountNearBombs.ToString() + " ";
		String_t* L_5 = V_0;
		CellScriptU5BU2CU5D_t9E37A76D72FEFD7DBD2886BFB77450DA8E962AB5* L_6 = __this->get_cellScripts_17();
		int32_t L_7 = V_1;
		int32_t L_8 = V_2;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_9;
		L_9 = (L_6)->GetAtUnchecked(L_7, L_8);
		int32_t* L_10 = L_9->get_address_of_CountNearBombs_13();
		String_t* L_11;
		L_11 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_10, /*hidden argument*/NULL);
		String_t* L_12;
		L_12 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(L_5, L_11, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0053;
	}

IL_0047:
	{
		// abc += "$ ";
		String_t* L_13 = V_0;
		String_t* L_14;
		L_14 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_13, _stringLiteral8CC2154BA99A342FCE979C29AE9C29FFDC415950, /*hidden argument*/NULL);
		V_0 = L_14;
	}

IL_0053:
	{
		// for (int j = 0; j < CountCellsInLine; j++)
		int32_t L_15 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0057:
	{
		// for (int j = 0; j < CountCellsInLine; j++)
		int32_t L_16 = V_2;
		int32_t L_17 = __this->get_CountCellsInLine_7();
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_000e;
		}
	}
	{
		// abc += "\n";
		String_t* L_18 = V_0;
		String_t* L_19;
		L_19 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_18, _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD, /*hidden argument*/NULL);
		V_0 = L_19;
		// for (int i = 0; i < CountCellsInColumn; i++)
		int32_t L_20 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1));
	}

IL_0070:
	{
		// for (int i = 0; i < CountCellsInColumn; i++)
		int32_t L_21 = V_1;
		int32_t L_22 = __this->get_CountCellsInColumn_8();
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_000a;
		}
	}
	{
		// print(abc);
		String_t* L_23 = V_0;
		MonoBehaviour_print_m4F113B89EC1C221CAC6EC64365E6DAD0AF86F090(L_23, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameController__ctor_m9D952052C0A7234373FA5531292FCA8855BE2643 (GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CellScript/<FlipAnim>d__32::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CFlipAnimU3Ed__32__ctor_m3C8306BE011D741F286EEF99EF338678674A5B20 (U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void CellScript/<FlipAnim>d__32::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CFlipAnimU3Ed__32_System_IDisposable_Dispose_mA38B1F7485A6DD070CDC1C9C30BF5A0890582A84 (U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean CellScript/<FlipAnim>d__32::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CFlipAnimU3Ed__32_MoveNext_mA5542445EF5C18ED485121A4D9886598BE1AA931 (U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_1 = __this->get_U3CU3E4__this_5();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
		// float deltaRot = (endRotation - startRotation) / countFrames;
		float L_4 = __this->get_endRotation_2();
		float L_5 = __this->get_startRotation_3();
		int32_t L_6 = __this->get_countFrames_4();
		__this->set_U3CdeltaRotU3E5__2_6(((float)((float)((float)il2cpp_codegen_subtract((float)L_4, (float)L_5))/(float)((float)((float)L_6)))));
		// for (int i = 0; i < countFrames; i++)
		__this->set_U3CiU3E5__3_7(0);
		goto IL_0088;
	}

IL_0042:
	{
		// transform.Rotate(0f, deltaRot, 0f);
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_7 = V_1;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_7, /*hidden argument*/NULL);
		float L_9 = __this->get_U3CdeltaRotU3E5__2_6();
		Transform_Rotate_mA3AE6D55AA9CC88A8F03C2B0B7CB3DB45ABA6A8E(L_8, (0.0f), L_9, (0.0f), /*hidden argument*/NULL);
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * L_10 = (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA(L_10, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_10);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0071:
	{
		__this->set_U3CU3E1__state_0((-1));
		// for (int i = 0; i < countFrames; i++)
		int32_t L_11 = __this->get_U3CiU3E5__3_7();
		V_2 = L_11;
		int32_t L_12 = V_2;
		__this->set_U3CiU3E5__3_7(((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1)));
	}

IL_0088:
	{
		// for (int i = 0; i < countFrames; i++)
		int32_t L_13 = __this->get_U3CiU3E5__3_7();
		int32_t L_14 = __this->get_countFrames_4();
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_0042;
		}
	}
	{
		// transform.rotation = Quaternion.Euler(0f, endRotation, 0f);
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_15 = V_1;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16;
		L_16 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_15, /*hidden argument*/NULL);
		float L_17 = __this->get_endRotation_2();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_18;
		L_18 = Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3((0.0f), L_17, (0.0f), /*hidden argument*/NULL);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_16, L_18, /*hidden argument*/NULL);
		// }
		return (bool)0;
	}
}
// System.Object CellScript/<FlipAnim>d__32::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CFlipAnimU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE78D8A10FFED8EA5C8F65572335539A4DD0B6364 (U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void CellScript/<FlipAnim>d__32::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CFlipAnimU3Ed__32_System_Collections_IEnumerator_Reset_m9346BED6567910CA6F876F1A961BB0EFB5B15431 (U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CFlipAnimU3Ed__32_System_Collections_IEnumerator_Reset_m9346BED6567910CA6F876F1A961BB0EFB5B15431_RuntimeMethod_var)));
	}
}
// System.Object CellScript/<FlipAnim>d__32::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CFlipAnimU3Ed__32_System_Collections_IEnumerator_get_Current_m208BCEA674C57BCBD6C31EF2D9FEAAAA47D199EE (U3CFlipAnimU3Ed__32_t4D975B8507A0B453325EC277863A63CA0BB0B8CC * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CellScript/<VibrateAnim>d__33::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVibrateAnimU3Ed__33__ctor_m1C4133168D5711366DB5305F2A4FBE525697AE9F (U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void CellScript/<VibrateAnim>d__33::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVibrateAnimU3Ed__33_System_IDisposable_Dispose_mFD1311276519EC7C7EF5ED437DA236ACA83C1E0B (U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean CellScript/<VibrateAnim>d__33::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CVibrateAnimU3Ed__33_MoveNext_m9A64EB46346DA54E238B8A45066763A79E81F4FF (U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE8391A0839948AFB6B9EF3EA9ECED133D95DF1B8);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0022;
			}
			case 1:
			{
				goto IL_00ad;
			}
			case 2:
			{
				goto IL_011b;
			}
		}
	}
	{
		return (bool)0;
	}

IL_0022:
	{
		__this->set_U3CU3E1__state_0((-1));
		// SetCellScale(1f);
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_3 = V_1;
		CellScript_SetCellScale_mE30F34C461AF6953FEAE12111E0690040C8EF5C0(L_3, (1.0f), /*hidden argument*/NULL);
		// float deltaScale = (1.1f - transform.localScale.x) / deltaFrames;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_4 = V_1;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5;
		L_5 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_4, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_5, /*hidden argument*/NULL);
		float L_7 = L_6.get_x_2();
		int32_t L_8 = __this->get_deltaFrames_3();
		__this->set_U3CdeltaScaleU3E5__2_5(((float)((float)((float)il2cpp_codegen_subtract((float)(1.10000002f), (float)L_7))/(float)((float)((float)L_8)))));
		// for (int i = 0; i < countFrames; i+=(deltaFrames*2))
		__this->set_U3CiU3E5__3_6(0);
		goto IL_0155;
	}

IL_0064:
	{
		// for (int j = 0; j < deltaFrames; j++)
		__this->set_U3CjU3E5__4_7(0);
		goto IL_00c4;
	}

IL_006d:
	{
		// transform.localScale += new Vector3(deltaScale, deltaScale, 0f);
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_9 = V_1;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_10;
		L_10 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_9, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11 = L_10;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_11, /*hidden argument*/NULL);
		float L_13 = __this->get_U3CdeltaScaleU3E5__2_5();
		float L_14 = __this->get_U3CdeltaScaleU3E5__2_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		memset((&L_15), 0, sizeof(L_15));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_15), L_13, L_14, (0.0f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_12, L_15, /*hidden argument*/NULL);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_11, L_16, /*hidden argument*/NULL);
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * L_17 = (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA(L_17, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_17);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_00ad:
	{
		__this->set_U3CU3E1__state_0((-1));
		// for (int j = 0; j < deltaFrames; j++)
		int32_t L_18 = __this->get_U3CjU3E5__4_7();
		V_2 = L_18;
		int32_t L_19 = V_2;
		__this->set_U3CjU3E5__4_7(((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1)));
	}

IL_00c4:
	{
		// for (int j = 0; j < deltaFrames; j++)
		int32_t L_20 = __this->get_U3CjU3E5__4_7();
		int32_t L_21 = __this->get_deltaFrames_3();
		if ((((int32_t)L_20) < ((int32_t)L_21)))
		{
			goto IL_006d;
		}
	}
	{
		// for (int j = 0; j < deltaFrames; j++)
		__this->set_U3CjU3E5__4_7(0);
		goto IL_0132;
	}

IL_00db:
	{
		// transform.localScale -= new Vector3(deltaScale, deltaScale, 0f);
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_22 = V_1;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_23;
		L_23 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_22, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_24 = L_23;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_25;
		L_25 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_24, /*hidden argument*/NULL);
		float L_26 = __this->get_U3CdeltaScaleU3E5__2_5();
		float L_27 = __this->get_U3CdeltaScaleU3E5__2_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28;
		memset((&L_28), 0, sizeof(L_28));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_28), L_26, L_27, (0.0f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29;
		L_29 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_25, L_28, /*hidden argument*/NULL);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_24, L_29, /*hidden argument*/NULL);
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * L_30 = (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA(L_30, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_30);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_011b:
	{
		__this->set_U3CU3E1__state_0((-1));
		// for (int j = 0; j < deltaFrames; j++)
		int32_t L_31 = __this->get_U3CjU3E5__4_7();
		V_2 = L_31;
		int32_t L_32 = V_2;
		__this->set_U3CjU3E5__4_7(((int32_t)il2cpp_codegen_add((int32_t)L_32, (int32_t)1)));
	}

IL_0132:
	{
		// for (int j = 0; j < deltaFrames; j++)
		int32_t L_33 = __this->get_U3CjU3E5__4_7();
		int32_t L_34 = __this->get_deltaFrames_3();
		if ((((int32_t)L_33) < ((int32_t)L_34)))
		{
			goto IL_00db;
		}
	}
	{
		// for (int i = 0; i < countFrames; i+=(deltaFrames*2))
		int32_t L_35 = __this->get_U3CiU3E5__3_6();
		int32_t L_36 = __this->get_deltaFrames_3();
		__this->set_U3CiU3E5__3_6(((int32_t)il2cpp_codegen_add((int32_t)L_35, (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_36, (int32_t)2)))));
	}

IL_0155:
	{
		// for (int i = 0; i < countFrames; i+=(deltaFrames*2))
		int32_t L_37 = __this->get_U3CiU3E5__3_6();
		int32_t L_38 = __this->get_countFrames_4();
		if ((((int32_t)L_37) < ((int32_t)L_38)))
		{
			goto IL_0064;
		}
	}
	{
		// SetCellScale(1f);
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_39 = V_1;
		CellScript_SetCellScale_mE30F34C461AF6953FEAE12111E0690040C8EF5C0(L_39, (1.0f), /*hidden argument*/NULL);
		// if (needToStartRestart)
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_40 = V_1;
		bool L_41 = L_40->get_needToStartRestart_18();
		if (!L_41)
		{
			goto IL_018f;
		}
	}
	{
		// Invoke("Lose_SecondStage", gc.AfterVibrationsRestartTime);
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_42 = V_1;
		CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * L_43 = V_1;
		GameController_tB9A9FA594113443BE7F9A0B6EC8588EB5101D274 * L_44 = L_43->get_gc_4();
		float L_45 = L_44->get_AfterVibrationsRestartTime_11();
		MonoBehaviour_Invoke_m4AAB759653B1C6FB0653527F4DDC72D1E9162CC4(L_42, _stringLiteralE8391A0839948AFB6B9EF3EA9ECED133D95DF1B8, L_45, /*hidden argument*/NULL);
	}

IL_018f:
	{
		// }
		return (bool)0;
	}
}
// System.Object CellScript/<VibrateAnim>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CVibrateAnimU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE1FC24545CDBE5DC666A9A403010C610013CCDC5 (U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void CellScript/<VibrateAnim>d__33::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CVibrateAnimU3Ed__33_System_Collections_IEnumerator_Reset_m88E0A911C29AA03E40B41362A615AC65E8D4798A (U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CVibrateAnimU3Ed__33_System_Collections_IEnumerator_Reset_m88E0A911C29AA03E40B41362A615AC65E8D4798A_RuntimeMethod_var)));
	}
}
// System.Object CellScript/<VibrateAnim>d__33::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CVibrateAnimU3Ed__33_System_Collections_IEnumerator_get_Current_mBD8DF7B95211DD97103BFB324079B71C24AEDE8D (U3CVibrateAnimU3Ed__33_t46DB9238D66AE371A979E9F1BEC6975E23E53AAE * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CellScript_setMarkedState_m464FBE7E2E19E707F61160CAF5D92FCFB82001A0_inline (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, bool ___newState0, const RuntimeMethod* method)
{
	{
		// IsMarked = newState;
		bool L_0 = ___newState0;
		__this->set_IsMarked_15(L_0);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CellScript_SetBombState_m34FA5F4F7839C8343B34D2AB0A2729B3EDB95F7C_inline (CellScript_tC9F4AB54F200D0F71B2817503CEFEB97FD095CAA * __this, bool ___newState0, const RuntimeMethod* method)
{
	{
		// IsBomb = newState;
		bool L_0 = ___newState0;
		__this->set_IsBomb_14(L_0);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_2, (int32_t)L_3);
		return (RuntimeObject *)L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
