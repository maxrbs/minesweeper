# Minesweeper
## Description
### RU
Простая версия игры Сапер.
Цель состоит в том, чтобы правильно указать на все клетки с бомбами, ориентируясь на числовые значения открытых ячеек — все так же, как и в оригинальной игре.
Управление мышью - ЛКМ и ПКМ.

### EN
A simple version of the Minesweeper game.
The goal is to correctly point to all cells with bombs, focusing on the number values of open cells - everything is the same as in the original game.
Mouse control - LMB and RMB.

## Screenshots
![Screen1](/Sharing/Screenshots/1.png)
![Screen2](/Sharing/Screenshots/2.png)

## Links
Build on itch: https://maxrbs.itch.io/minesweeper